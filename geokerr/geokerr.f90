       program TESTGEOPHIT
       implicit none
!    gfortran -ffixed-line-length-132 -O3 geokerr.f -o geokerr
! This program calculates all coordinates of null geodesics in a Kerr spacetime semi-analytically.
! Based off the paper Dexter & Agol (2009). If you make use of this program or any of the following routines,
! please cite this paper.
! ******************************************************************
!----------------------- Variable definitions ---------------------------
      double precision a,alpha,beta,l,l2,one,phi,pi,q2,offset,roffset,z1,z2,rms,tres,third, &
                       alphay,gamma,du,ub,input_limit,abmax,fac,tref,lambdaref,dumax
      integer n,ncase,nko,ndisk,nphi,nro,i,ii,kmax,nupsave, &
              j,k,nrotype,nup,ngeo,standard,ql,notpr,nmin,nmax,maxel,maxnro
! NRO*NPHI is the total number of geodesics for a standard run. If a circular grid is used, NRO
! is the number of evenly spaced rho=sqrt(alpha^2+beta^2) points, while NPHI is the number of 
! phi=atan(alpha/beta) points to calculate. If a rectangular grid is used, NRO is the number of evenly
! spaced points in alpha and NPHI is the number in beta. FAC determines the observer radius when the input 
! is infinity--fac should be >> 1 to ensure that the portion of geodesic from infinity to new observer radius
! varies little from its Minkowski counterpart.
! Input values of q2 less than INPUT_LIMIT^2 or of a less than INPUT_LIMIT are set to zero.
! OFFSET is the amount that the calculated uf differs from the input uf, normalized by the step size in u. 
      parameter (fac=100.D0,input_limit=1.D-5,maxel=5000,maxnro=5000)
      double precision amin,amax,bmin,bmax,gn,mu0,r1,r2,rcut,ro(maxnro), &
             sm,su,wro(maxnro),wa,wb,h1,nu,intensity,phii
      double precision a1,a2,b1,b2,u1,u2,u3,u4,delta,time,time0, &
             phiu,tu,two,wphi,zero,rffu0,rffu1,rffmu1,rffmu2,rffmu3,tu01,tu02,tu03,tu04, &
             iu0,i1mu,i3mu,tmu1,tmu3,phimu1,phimu3,rdc,rjc
      double precision umini,uplus,u0,uf,muf,iu,uc,un,mun,iuc,phimu,tmu,lambda, &
                       ufi(maxel),mufi(maxel),dti(maxel),dphi(maxel),lambdai(maxel)
      integer tpmi(maxel),tpri(maxel)
!*************************************************************** 
      double precision a1t,a2t,b1t,b2t,hc,hk,small,wat,wbt,g,uout
      character input_save*20, outfile*20, infile*20
!*************************************************************** 
!-  Set up parameters: 
      parameter ( zero=0.D0, one=1.D0, two=2.D0, tres=3.D0, third=1.D0/3.D0 )
      integer tpr1,tpr,tpm,ns,outunit,inunit,saveinp, terminal
!   Set NUP=1 by default:
      nup=1
!   Set OFFSET=.5 by default, ie final values at midpoints between (final-initial)/number steps.
      offset=.5D0
!   By default, output is sent to terminal. It is advised that either an output file is supplied,
!   or that output is redirected to a file through the terminal.
!   If OUTUNIT=6, the output is directed to standard output (the terminal). Otherwise, output is written
!   to the file OUTFILE.
      outunit=6
      outfile='default.out'
      if(outunit.ne.6) open(unit=outunit,file=outfile)
      inunit=7
      infile='default.in'
      if(inunit.ne.5) open(unit=inunit,file=infile)
      pi=ACOS(-one)
      read(inunit,*) STANDARD
!-  A standard run assumes inputs will be over a supported grid style in alpha, beta.
!   If STANDARD=0, arrays of input parameters are read in without the standard assumptions.
!   If STANDARD=99, it is assumed the user is entering inputs manually. 
!   These can be saved to a file if desired.
      if(standard.eq.0) then
        read(inunit,*) QL
        read(inunit,*) NGEO
        read(inunit,*) A
        read(inunit,*) NUP
      else
        ngeo=nro*nphi
        if(standard.eq.99) then
!   At least for now require prompted input to be standard.
	    standard=1
10        write(6,*) 'Dimensionless black hole spin: '
          read(5,*) A
          if(ABS(a).ge.one) then
            write(6,*) 'Spin must be between -1, 1.'
            goto 10
          endif
20          write(6,*) 'Initial polar angle: '
          read(5,*) MU0
          if(ABS(mu0).gt.one) then
            write(6,*) 'Polar angle must be between -1, 1.'
            goto 20
          endif
          write(6,*) &
               'Type of grid at infinity: Circular (1), Square (2)'
          read(5,*) NROTYPE
          if(nrotype.eq.1) then
            write(6,*) 'Outer radius of grid at infinity: '
            read(5,*) RCUT
            abmax=rcut**2
          else
            write(6,*) &
          'Grid boundaries as Alpha Min, Alpha Max, Beta Min, Beta Max:'
            read(5,*) A1,A2,B1,B2
            abmax=MAX(a1*a1,a2*a2)**2+MAX(b1*b1,b2*b2)**2
          endif
          write(6,*)'Number of geodesics as NRO, NPHI       for circular grid or NX, NY for square:'
          read(5,*) NRO,NPHI
          write(6,*) 'Number of points to compute per geodesic:'
          read(5,*) NUP
          write(6,*) 'Save inputs for later use? (1 Yes, 0 No)'
          read(5,*) SAVEINP
          if(saveinp.ne.0) then
            write(6,*) 'Enter name of input file to save:'
            read(5,*) INPUT_SAVE
            open(unit=12,file=input_save)
            write(12,*) standard
            write(12,*) mu0
            write(12,*) a
            write(12,*) rcut
            write(12,*) nrotype
            write(12,*) a1,a2,b1,b2
            write(12,*) nro,nphi,nup
            close(unit=12)
          endif
          write(6,*) 'Output to terminal or file? (1 Terminal)'
          read(5,*) TERMINAL
          if(terminal.ne.1) then
            outunit=12
            write(6,*) 'Name of output file?'
            read(5,*) OUTFILE
            open(file=outfile,unit=outunit)
          endif
        else
!-  Read in the observation angle:
          read(inunit,*) MU0
!   a is the spin of the black hole in geometrized units:
          read(inunit,*) A
!  RCUT is the outer boundary of the grid at infinity:
          read(inunit,*) RCUT
!-------------- Observation radius - grid of integration -----------
          read(inunit,*) NROTYPE
          read(inunit,*) A1,A2,B1,B2
          read(inunit,*) NRO,NPHI,NUP
        endif
!  For NROTYPE=1, use a circular grid:
        if(nrotype.eq.1) then
!  The range of observed radii I'm taking to be logarithmically spaced
!  from R=0 to RCUT:
          abmax=rcut**2
          r1=zero 
          r2=LOG(two)
          call GAULEG(r1,r2,ro,wro,nro,nro)
          do 5 i=1,nro
            ro(i)=rcut*(EXP(RO(i))-one)
5         continue
!  If NROTYPE > 1, use a rectangular grid:
        else
! A1,A2 and B1,B2 are the upper and lower limits for alpha & beta, respectively:
          wa=(a2-a1)/DBLE(nro)
          wb=(b2-b1)/DBLE(nphi)
          abmax=MAX(a1*a1,a2*a2)**2+MAX(b1*b1,b2*b2)**2
        endif
        ngeo=nro*nphi
      endif  
      uplus=one/(one+SQRT(one-a*a))
      umini=(one-SQRT(one-a*a))
! Since we don't want a divergence of the time, let's make 1/U0 finite:
      u0=MIN(1.D-4,one/(fac*abmax))
!   Write base parameters to file
! If NUP=1, let final value computed be uf/muf as input, without trying to calculate at the horizon:
      if(nup.eq.1) offset=1.D-8
      if(standard.ne.0) then
        write(outunit,*) ngeo,mu0,a,u0
      else
! If STANDARD=0, U0 and MU0 is allowed to change with each geodesic.
        write(outunit,*) ngeo,a
      endif
      if(standard.eq.2) then
!  When STANDARD=2, we solve for uf given u0, mu0 and muf.
        do 50 ii=1,ngeo
!-  Loop over all geodesics:
          i=INT(DBLE(ii-1)/DBLE(nphi))+1
          j=MOD(ii-1,nphi)+1
          if(nrotype.gt.1) then
            alpha=a1+(a2-a1)*(DBLE(i-1)+0.5D0)/DBLE(nro)
            beta=b1+(b2-b1)*(DBLE(j-1)+0.5D0)/DBLE(nphi)
          else
            if(nphi.ne.1.D0) then 
              phi=two*pi*DBLE(j-1)/DBLE(nphi-1)
            else
              phi=0.D0
            endif
! Calculate impact parameters at infinity for observer at phi0=-pi/2:
            alpha=RO(i)*SIN(phi)       
            beta=-RO(i)*COS(phi)
          endif
! ******************************************************************
! Calculate the angular momentum and Carter's constant of motion:
          l=-alpha*SQRT(one-mu0*mu0)
          l2=l*l
          q2=beta*beta-(a*a-alpha*alpha)*mu0*mu0
! If any inputs are too small, set them to zero.
          if(ABS(q2).lt.input_limit**2) q2=zero
          if(ABS(a).lt.input_limit) a=zero
          if(ABS(l).lt.input_limit) l=zero
!  The sign of the U integral equals du/dlambda:
          su=one
!   If beta>0, then mu starts out moving positively if u0 is far from the black hole and su=one
          if(beta.gt.zero.and.mu0.lt.one) then
            sm=one
          else
            sm=-one
          endif
!  This method for calculating TPM works for ingoing geodesics when muf is near the equatorial plane.
          tpm=(SIGN(1.D0,mu0)*sm+one)/two
          muf=0.D0
! GEOKERR computes geodesic coordinates and number of turning points at nup points from mu0 to muf.
          call GEOKERR(u0,uf,uout,mu0,muf,a,l,q2,alpha,beta,tpm,tpr,su,sm,nup,offset,.true.,.true.,.false.,ncase, &
                       ufi,mufi,dti,dphi,tpmi,tpri,lambdai)
! Write geodesic info to file.
          write(outunit,*) alpha,beta,nup,ncase 
          write(outunit,200) UFI(1),MUFI(1),DTI(1),DPHI(1),LAMBDAI(1),TPMI(1),TPRI(1)
          do 45 k=2,nup
            write(outunit,200) UFI(k),MUFI(k),DTI(k)-DTI(1),DPHI(k),LAMBDAI(k)-LAMBDAI(1),TPMI(k),TPRI(k)
45        continue
50      continue
      else
!-  This is the standard case, where we solve for muf given u0, uf and mu0. 
!   Loop over all geodesics:
      nupsave=nup
        do 100 ii=1,ngeo
          if(standard.eq.1) then
! UF takes even steps from UOUT to horizon or to turning point and back to UOUT (for full geodesic set UOUT=U0):
!            UOUT=1.D0/25.D0
            uout=u0
            uf=uplus
! Setting TPR=1 doesn't mean that a turning point is necessarily present, but that if one is we calculate points past it.
            tpr=1
            i=INT(DBLE(ii-1)/DBLE(nphi))+1
            j=MOD(ii-1,nphi)+1
            if(nrotype.gt.1) then
              alpha=a1+(a2-a1)*(DBLE(i-1)+0.5D0)/DBLE(nro)
              beta=b1+(b2-b1)*(DBLE(j-1)+0.5D0)/DBLE(nphi)
            else
              if(nphi.ne.1.D0) then 
                phi=two*pi*DBLE(j-1)/DBLE(nphi-1)
              else
                phi=0.D0
              endif
! Calculate impact parameters at infinity:
              alpha=RO(i)*SIN(phi)
              beta=-RO(i)*COS(phi)
            endif
! ******************************************************************
! Calculate the angular momentum and Carter's constant of motion:
            l=-alpha*sqrt(one-mu0*mu0)
            l2=l*l
            q2=beta*beta-(a*a-alpha*alpha)*mu0*mu0
!  The sign of the U integral equals du/dlambda:
            su=one
!   If beta>0, then dmu/dlambda > 0  if u0 is far from the black hole and su=one
            if(mu0.lt.one.and.beta.ge.zero) then
              sm=one
            else
              sm=-one
            endif
          else
            if(ql.eq.0) then
              read(inunit,*) ALPHA, BETA, U0, MU0, UF, SU, TPR
              uout=u0
! Calculate the angular momentum and Carter's constant of motion:
              l=-su*alpha*sqrt(one-mu0*mu0)
              l2=l*l
              q2=beta*beta-(a*a-alpha*alpha)*mu0*mu0
!   If beta>0, then mu starts out moving positively if u0 is far from the black hole and su=one
              if(mu0.lt.one.and.beta.gt.zero) then
                sm=one
              else
                sm=-one
              endif
            else
              read(inunit,*) Q2,L,U0,MU0,UF,SU,TPR
! Check that constants are physical:
              if(q2.lt.(mu0*mu0*(l*l/(1.D0-mu0*mu0)-a*a))) then
                write(6,*) 'Invalid inputs will be ignored.'
                goto 100
              endif
! In this case we want to output the given constants, and alpha and beta are no longer 
! used for any other purpose.
              alpha=q2
              beta=l
              l2=l*l
              sm=-su
            endif
          endif
          if(ABS(q2).lt.input_limit**2) q2=zero
          if(ABS(a).lt.input_limit) a=zero
          if(ABS(l).lt.input_limit) l=zero
          tpm=0
! Geokerr calculates NUP points at evenly spaced U between UOUT and UF if TPR=0, or between UOUT,UB and UB,UF if one is.
          call GEOKERR(u0,uf,uout,mu0,muf,a,l,q2,alpha,beta,tpm,tpr,su,sm,nup,offset,.true.,.false.,.false.,ncase, &
                       ufi,mufi,dti,dphi,tpmi,tpri,lambdai)
          write(outunit,*) alpha,beta,nup,u0,ncase
          if(nup.ne.0) then
! Write first point separately to set reference time, affine parameter.
            write(outunit,200) UFI(1),MUFI(1),DTI(1),DPHI(1),LAMBDAI(1),TPMI(1),TPRI(1)
            do 99 k=2,nup
! Output dt, lambda with respect to the first value so that when u0 is very small, minimal precision is lost
! in output.
              write(outunit,200) UFI(k),MUFI(k),DTI(k)-DTI(1),DPHI(k),LAMBDAI(k)-LAMBDAI(1),TPMI(k),TPRI(k)
99          continue
          endif
! Reset nup value in case it's changed.
          nup=nupsave
100     continue          
      endif
200   format(5(1x,1pe16.8),2(I2))
      end
!*********************************************************************************************************
      subroutine GEOKERR(u0,uf,uout,mu0,muf,a,l,q2,alpha,beta,tpm,tpr,su,sm,nup,offset,phit,usegeor,mufill,ncase, &
                         ufi,mufi,dti,dphi,tpmi,tpri,lambdai)
!*********************************************************************************************************
!     PURPOSE: Computes nup points on the null geodesic specified by q2, l in the Kerr metric with 
!              spin parameter a.
!
!     INPUTS:   U0 -- Starting u value. If U0=0, DTI and LAMBDAI values will blow up.
!               UF -- Final u value, either given if USEGEOR=.FALSE., or computed if USEGEOR=.TRUE.
!               UOUT -- Computed u values are picked between UOUT and UF. For the full geodesic,
!                       set U0=UOUT.
!               MU0 -- Starting mu=cos(theta) value.
!               MUF -- Final mu value, given if USEGEOR=.TRUE. and computed otherwise.
!               A -- Black hole spin, on interval [0,1).
!               L -- Dimensionless z-component of angular momentum.
!               Q2 -- Dimensionless Carter's constant.
!               ALPHA -- Impact parameter at infinity perpendicular to the black hole spin axis. alpha=0
!                        has l=0.
!               BETA -- Impact parameter at infinity parallel to the black hole spin axis. beta=0,
!                       mu0=0 gives geodesics in the equatorial plane. 
!               TPM -- Number of mu turning points present. Computed when USEGEOR=.FALSE., provided
!                      otherwise. Note that at present Geokerr does not support USEGEOR=.TRUE. when tracing
!                      through a region where the number of mu turning points changes.
!               SU -- Initial du/dlambda, =1 (-1) for ingoing (outgoing) rays.
!               SM -- Initital dmu/dlambda, can be computed from known mu0, beta if 1/u0 is large.
!               NUP -- Number of points to calculate.
!               OFFSET -- Offset of value of uf or muf calculated from the one supplied, normalized by NUP.
!                         When NUP=1, OFFSET=0 gives the point uf or muf.
!               PHIT -- Boolean variable. If .TRUE., DTI and DPHI are calculated.
!               USEGEOR -- Boolean variable determining whether we solve for UF or MUF.
!               MUFILL -- Boolean variable to determine if we switch to mu as the independent variable near u turning points.
!     OUTPUTS:  UFI(NUP)  -- array of UF values between u0, uf and if TPR=1 between u0, ub, uf
!               MUFI(NUP) -- array of MUF values corresponding to UFI
!               DTI(NUP)  -- array of delta t values corresponding to T(UFI(NUP))-T(U0)
!               DPHI(NUP) -- array of delta phi values corresponding to PHI(UFI(NUP))-PHI(U0)
!               TPMI(NUP) -- array of mu turning point values, corresponding to the number of mu turning 
!                            points encountered between u0 and UFI(I).
!               TPRI(NUP) -- array of u turning point values, corresponding to the number of u turning
!                            points encountered between u0 and UFI(I).
!               LAMBDAI(NUP) -- array of affine parameter values corresponding to LAMBDA(UFI(NUP)).
!     ROUTINES CALLED:  GEOMU, GEOR, GEOPHITIME
!     ACCURACY:   Machine.
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: --Added MUFILL option to increase geodesic resolution near u turning points.
!*********************************************************************************************************
      implicit none
      logical phit,usegeor,firstpt,mufill
      integer nup,ncase,kmax,k,nmax,tpm,tpr,tpr1,npts,tpm1,kext
      double precision u0,uf,un,mu0,mun,muf,a,l,l2,q2,su,sm,smu,one,ub,du,dumax, &
                     iu,h1,u1,u2,u3,u4,rffu0,rffu1,iu0,i1mu,i3mu,alpha,beta,lambda,uout, &
                     offset,tu,tmu,phiu,phimu,rffmu1,rffmu2,rffmu3,phimu1,phimu3,tmu1,tmu3, &
                     rdc,rjc,tu01,tu02,tu03,tu04,two,uplus,muminus,muplus
      parameter (one=1.D0,two=2.D0,nmax=5000)
      double precision ufi(nmax),mufi(nmax),dti(nmax),dphi(nmax),lambdai(nmax)
      integer tpmi(nmax),tpri(nmax)
      l2=l*l
      npts=0
      kext=0
      firstpt=.false.
      uplus=one/(one+SQRT(one-a*a))
      if(usegeor) then
! Find roots of M(mu):
        call FINDMROOTS(q2,l,a,mu0,muminus,muplus)
        k=1
! Solve for uf at equal steps between mu0, muf by calling geor assuming no mu turning points are present:
!        MUN=MU0+(K-OFFSET)*(MUF-MU0)/DBLE(NUP)
        call INDEP_MUF(muminus,muplus,mu0,muf,0,tpm,sm,k,nup,offset,mun,tpm1)
        if(mu0.ne.0.D0.or.beta.ne.0.D0) then
          call GEOR(u0,uf,mu0,mun,a,l,l2,q2,iu,tpm1,tpr,su,sm,ncase,h1, &
                    u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,phit,.true.)
          if(phit) call GEOPHITIME(u0,uf,mu0,mun,a,l,l2,q2,tpm1,tpr,su,sm,iu,h1, &
                 phimu,tmu,ncase,u1,u2,u3,u4,phiu,tu,lambda,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
                 rdc,rjc,tu01,tu02,tu03,tu04,tmu1,tmu3,phimu1,phimu3,.true.)
        else
          uf=u0
        endif
        ufi(k)=uf
        mufi(k)=mun
        dti(k)=tmu+tu
        dphi(k)=phimu+phiu
        tpmi(k)=tpm1
        tpri(k)=tpr
        lambdai(k)=lambda
        do 75 k=2,nup
!         MUN=MU0+(K-OFFSET)*(MUF-MU0)/DBLE(NUP)
          call INDEP_MUF(muminus,muplus,mu0,muf,0,tpm,sm,k,nup,offset,mun,tpm1)
          if(mu0.ne.0.D0.or.beta.ne.0.D0) then
          call GEOR(u0,uf,mu0,mun,a,l,l2,q2,iu,tpm1,tpr,su,sm,ncase,h1, &
                    u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,phit,.false.)
          if(phit) call GEOPHITIME(u0,uf,mu0,mun,a,l,l2,q2,tpm1,tpr,su,sm,iu,h1, &
                 phimu,tmu,ncase,u1,u2,u3,u4,phiu,tu,lambda,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
                 rdc,rjc,tu01,tu02,tu03,tu04,tmu1,tmu3,phimu1,phimu3,.true.)
          else
            uf=u0
          endif
          ufi(k)=uf
          mufi(k)=mun
          dti(k)=tmu+tu
          dphi(k)=phimu+phiu
          tpmi(k)=tpm1
          tpri(k)=tpr
          lambdai(k)=lambda
75      continue
      else
! Modify input parameters if necessary, get appropriate case and roots.
        call GEOMU(u0,uf,mu0,muf,a,l,l2,q2,iu,tpm,tpr,su,sm,ncase,h1, &
                  u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,phit,.true.)
        if(ncase.gt.2.and.ncase.lt.7.or.(ncase.eq.1.and.su.lt.0.D0).or.(ncase.eq.7.and.su.lt.0.D0).or. &
          (ncase.eq.2.and.su.gt.0.D0).or.(ncase.eq.8.and.su.gt.0.D0)) tpr=0
        if(ncase.gt.2.and.ncase.lt.7) then
          if(su.gt.0.D0) then
            ub=uplus
          else
            ub=0.D0
          endif
        elseif(ncase.eq.1.or.ncase.eq.7) then
          ub=u2
          if(tpr.eq.1.and.uf.eq.ub) uf=uout
        else
          ub=u3
          if(tpr.eq.1.and.uf.eq.ub) uf=uout
        endif
        tpr1=0
        du=SIGN(one,ub-uout)*su*((ub-uout)+(two*tpr-one)*(ub-uf))
        du=du/DBLE(nup)
        kmax=MIN(INT((ub-uout)/du+offset),nup)
        if(du.eq.0.D0) kmax=0
        if(SIGN(1.D0,uout-ub).ne.SIGN(1.D0,uout-u0).or.(uout.eq.u0)) then
          if(kmax.ne.0) then
! Do the first point separately to compute once per geodesic integrals in GEOPHITIME:
            k=1
            un=uout+(k-offset)*du
            call GEOMU(u0,un,mu0,mun,a,l,l2,q2,iu,tpm,tpr1,su,sm,ncase,h1, &
                   u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu, &
                   phit,.false.)
            if(phit) call GEOPHITIME(u0,un,mu0,mun,a,l,l2,q2,tpm,tpr1,su,sm,iu,h1, &
                 phimu,tmu,ncase,u1,u2,u3,u4,phiu,tu,lambda,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
                 rdc,rjc,tu01,tu02,tu03,tu04,tmu1,tmu3,phimu1,phimu3,.true.)
            ufi(k)=un
            mufi(k)=mun
            dti(k)=tmu+tu
            dphi(k)=phimu+phiu
            tpmi(k)=tpm
            tpri(k)=0
            lambdai(k)=lambda
          endif
! First, trace from u0 to uf or turning point:
          do 1000 k=2,kmax
            un=uout+(k-offset)*du
            call GEOMU(u0,un,mu0,mun,a,l,l2,q2,iu,tpm,tpr1,su,sm,ncase,h1, &
                   u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu, &
                   phit,.false.)
            if (phit) call GEOPHITIME(u0,un,mu0,mun,a,l,l2,q2,tpm,tpr1,su,sm,iu,h1, &
                 phimu,tmu,ncase,u1,u2,u3,u4,phiu,tu,lambda,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
                 rdc,rjc,tu01,tu02,tu03,tu04,tmu1,tmu3,phimu1,phimu3,.false.)
            ufi(k)=un
            mufi(k)=mun
            dti(k)=tmu+tu
            dphi(k)=phimu+phiu
            tpmi(k)=tpm
            tpri(k)=0
            lambdai(k)=lambda
1000      continue
          if(tpr.eq.1) then
            if(nup.eq.1) firstpt=.true.
            if(mufill) then
! This option uses GEOR to fill in points near u turning point, where tiny steps in u lead to large steps in mu.
! First, calculate muf and tpm on the opposite side of the u turning point:
              kext=8
              k=kmax+kext+1
              npts=120
              un=two*ub-(uout+(k-offset)*du)
              call GEOMU(uf,un,mu0,mun,a,l,l2,q2,iu,tpm1,tpr,su,sm,ncase,h1, &
                    u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,phit,.false.)
              muf=mun
! Get roots of M(mu):
              call FINDMROOTS(q2,l,a,mu0,muminus,muplus)
              do 1500 k=1,npts
                call INDEP_MUF(muminus,muplus,MUFI(kmax-kext),muf,TPMI(kmax-kext),tpm1,sm,k,npts,offset,mun,tpm)
                call GEOR(u0,un,mu0,mun,a,l,l2,q2,iu,tpm,tpr,su,sm,ncase,h1, &
                    u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,phit,.false.)
                call GEOPHITIME(u0,un,mu0,mun,a,l,l2,q2,tpm,tpr,su,sm,iu,h1, &
                    phimu,tmu,ncase,u1,u2,u3,u4,phiu,tu,lambda,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
                    rdc,rjc,tu01,tu02,tu03,tu04,tmu1,tmu3,phimu1,phimu3,.false.)
                ufi(k+kmax-kext)=un
                mufi(k+kmax-kext)=mun
                dti(k+kmax-kext)=tmu+tu
                dphi(k+kmax-kext)=phimu+phiu
                tpmi(k+kmax-kext)=tpm
                tpri(k+kmax-kext)=tpr
                lambdai(k+kmax-kext)=lambda
1500          continue
              npts=npts-2*kext
            endif
! Now, we'll trace from the turning point, if present, back to uf:
            do 2000 k=kmax+1+kext,nup
              un=two*ub-(uout+(k-offset)*du)
              call GEOMU(uf,un,mu0,mun,a,l,l2,q2,iu,tpm,tpr,su,sm,ncase,h1, &
                    u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,phit,firstpt)
              if(phit) call GEOPHITIME(uf,un,mu0,mun,a,l,l2,q2,tpm,tpr,su,sm,iu,h1, &
                    phimu,tmu,ncase,u1,u2,u3,u4,phiu,tu,lambda,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
                    rdc,rjc,tu01,tu02,tu03,tu04,tmu1,tmu3,phimu1,phimu3,firstpt)
              ufi(k+npts)=un
              mufi(k+npts)=mun
              dti(k+npts)=tmu+tu
              dphi(k+npts)=phimu+phiu
              tpmi(k+npts)=tpm
              tpri(k+npts)=1
              lambdai(k+npts)=lambda
2000        continue
            nup=nup+npts
          endif
        else
          nup=0
        endif
      endif 
      return
      end
!*******************************************************************************
        subroutine FINDMROOTS(q2,l,a,mu0,muminus,muplus)
!*******************************************************************************
!     PURPOSE: Find roots of M(mu) (Eq. (12)) when a ne 0, q2 ne 0.
!     INPUTS: Q2,L,A -- Constants of the motion
!             MU0 -- Initial mu.
!     OUTPUTS: MUMINUS, MUPLUS -- Lower and upper mu turning points.
!     ROUTINES CALLED: *
!     ACCURACY: Machine.
!     AUTHOR: Dexter & Agol (2009)
!     DATE WRITTEN: 3/9/2009
!     REVISIONS: ***************************************************************
        double precision q2,l,a,mu0,muminus,muplus,yy,ql2,mneg,mpos
        ql2=q2+l*l 
        yy=-0.5d0*(a*a-ql2+sign(1.d0,a*a-ql2)*sqrt((a*a-ql2)**2+4.d0*q2*a*a))
        if((a*a-ql2).lt.0.d0) then
          mneg=-yy/a/a
          mpos=q2/yy
        else
          mneg=q2/yy
          mpos=-yy/a/a
        endif
        mpos=MIN(mpos,1.D0)
        if(mneg.gt.0.D0) then
! This is the asymmetric roots case.
          muplus=sign(1.d0,mu0)*sqrt(mpos)
          muminus=sign(1.d0,mu0)*sqrt(mneg)
          if(abs(muminus).gt.abs(mu0)) muminus=mu0
          if(abs(mu0).gt.abs(muplus)) muplus=mu0
        else
! This is the symmetric roots case, where the orbit can cross the equatorial plane.
          muplus=SQRT(mpos)
          if(muplus.lt.mu0) muplus=mu0
          muminus=-muplus
        endif
        return
        end
!************************************************************************************
      subroutine INDEP_MUF(muminus,muplus,mu0,muf,tpm0,tpmf,sm,k,npts,offset,mun,tpmk)
!************************************************************************************
!     PURPOSE: Computes appropriate MUF for use as independent variable for an arbitrary
!              number of mu turning points.
!
!     INPUTS: MUMINUS,MUPLUS -- Physical turning points.
!             MU0, MUF -- Initial and final mu. Values will be traced from MU0 to MUF
!                         including the appropriate number of turning points.
!             TPM0, TPMF -- Number of turning points encountered between starting MU and MU0,MUF.
!                           If MU0 is the initial MU, TPM0=0 and TPMF=TPM.
!             SM -- Initial sign of MU.
!             K -- Index for current MUF to find, between 1 and NPTS.
!             NPTS -- Total number of points between MU0 and MUF.
!     
!     OUTPUTS: MUN -- Appropriate MU value at index K.
!              TPMK -- Number of turning points at index K.
!     ROUTINES CALLED: *
!     ACCURACY: Machine.
!     AUTHOR: Dexter & Agol (2009)
!     DATE WRITTEN: 3/9/2009
!     REVISIONS:****************************************************************
            double precision muminus,muplus,mu0,muf,dmu,dmu1,dmu2,dmuk,mu1, &
                             mu2,mub,offset,sm,mun
            integer tpm0,tpmf,k,npts,a1,a2,a3,a4,dtpm,tpmk,dmukdmu1
! First calculate total path length:
            dtpm=tpmf-tpm0
!            IF(DTPM.GT.1) WRITE(6,*) 'Warning -- TPM diff > 1'
            a1=sm*(-1.D0)**tpm0
            a2=sm*(-1.D0)**tpmf
! Figure out if the next turning point reached is MUMINUS or MUPLUS:
            mu1=(a1+1.D0)/2.D0*muplus+(1.D0-a1)/2.D0*muminus
            mu2=(1.D0-a1)/2.D0*muplus+(a1+1.D0)/2.D0*muminus
            dmu1=ABS(mu1-mu0)
            dmu2=ABS(mu2-mu1)
! Eq. (35)
            a3=2*INT((2.D0*DBLE(dtpm)+3.D0-a1)/4.D0)-1
            dmu=DBLE(a1)*(muplus-mu0)+DBLE(a2)*(muf-muminus)+DBLE(a3)*(muplus-muminus)
!            WRITE(6,*) 'DMU: ',DMU,MUF-MU0,MUPLUS,MU0,MUMINUS,MUF
! Now get number of turning points between MU0 and our current index:
            dmuk=DBLE(k)*dmu/DBLE(npts)
            dmukdmu1=INT(dmuk/dmu1)
            if(INT(dmukdmu1).lt.0) dmukdmu1=50 
            tpmk=MIN(dmukdmu1,1)+MAX(INT((dmuk-dmu1)/dmu2),0)
!            WRITE(6,*) 'TPMK: ',TPMK,DMUK/DMU1,ABS(DMUK/DMU1),INT(ABS(DMUK/DMU1))
            a4=sm*(-1.D0)**(tpmk+tpm0)
            mub=mu1*MOD(tpmk,2)+mu0*((SIGN(1.D0,.5D0-tpmk)+1.D0)/2.D0)+mu2* &
                MOD(tpmk-1,2)*(SIGN(1.D0,tpmk-.5D0)+1.D0)/2.D0
            kmax1=INT(dmu1/dmu*npts)+1
            kmax2=INT(dmu2/dmu*npts)+1
!            WRITE(6,*) 'A4: ',TPMK,A4
            keff=k-(SIGN(1,k-kmax1)+1.D0)/2.D0*INT(dmu1/dmu*npts)- &
              (INT(dmu2/dmu*npts)+1)*INT((k-kmax1)/DBLE(kmax2))*(SIGN(1,k-kmax1-kmax2)+1.D0)/2.D0
            mun=mub+a4*(keff-offset)*dmu/DBLE(npts)
!            WRITE(6,*) 'KEFF: ',KEFF,DMUK,DMU,DMU1,DMU2
!            WRITE(6,*) 'COEFS: ',MU1,MU2,A1,A2,A3,DTPM,TPMF,TPM0,SM,TPMK
!            WRITE(6,*) 'INDEP_MUF: ',MUN,MUB,KEFF,OFFSET,DMU,NPTS,K
            tpmk=tpmk+tpm0
           return
           end
            !*****************************************************************************
      subroutine geomu(u0,uf,mu0,muf,a,l,l2,q2,iu,tpm,tpr,su,sm,ncase,h1, &
           u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,pht,firstpt)
!******************************************************************************
!     PURPOSE: Computes final polar angle muf at an inverse radius uf given initial inverse radius
!               and polar angle, and geodesic constants of the motion.
!
!     INPUTS:   U0 -- Starting u value. If U0=0, DTI and LAMBDAI values will blow up.
!               UF -- Final u value.
!               MU0 -- Starting mu=cos(theta) value.
!               A -- Black hole spin, on interval [0,1).
!               L -- Dimensionless z-component of angular momentum.
!               L2 -- L*L
!               Q2 -- Dimensionless Carter's constant.
!               TPR -- Number of u turning points reached between U0 and UF.
!               SU -- Initial du/dlambda, =1 (-1) for ingoing (outgoing) rays.
!               SM -- Initital dmu/dlambda.
!               PHT -- Boolean variable. If .TRUE., RFFMU2 is computed for use in
!                      SUBROUTINE GEOPHITIME.
!               FIRSTPT -- Boolean variable. If .TRUE., roots of U(u) U1,2,3,4 and NCASE are computed as well as H1
!                           if NCASE=6
!     OUTPUTS:  MUF -- Final polar angle.
!               IU -- Value of IU integral between U0 and UF.
!               TPM -- Number of mu turning points reached between MU0 and MUF.
!               NCASE -- Case number corresponding to Table 1. Output if FIRSTPT=.TRUE., input otherwise.
!               H1 -- Value of h1 from Eq. (21) for given constants of motion if NCASE=6. Output if FIRSTPT=.TRUE.,
!                     input otherwise.
!               U1,U2,U3,U4 -- Increasing (real) roots. Output if FIRSTPT=.TRUE., input otherwise.
!               RFFU0 -- Value of RF relevant for U0 piece of IU integral. Computed if FIRSTPT=.TRUE., input otherwise.
!               RFFU1 -- Value of RF relevant for UF piece of IU integral. Computed if PHT=.TRUE.
!               RFFMU1 -- Value of RF relevant for MU0 piece of IMU integral. Computed if FIRSTPT=.TRUE., input
!                         otherwise.
!               RFFMU2 -- Value of RF relevant for MUF piece of IMU integral. Computed if PHT=.TRUE.
!               RFFMU3 -- Value of RF relevant for turning point piece of IMU integral. Computed if FIRSTPT=.TRUE.,
!                         input otherwise.
!               IU0 -- Value of IU integral between U0 and relevant turning point if one exists. Computed if
!                      u turning point present and FIRSTPT=.TRUE., input otherwise. Ignored if no turning point
!                      is present.
!               I1MU -- Value of IMU integral between MU0 and MUPLUS. Computed if FIRSTPT=.TRUE., input otherwise.
!               I3MU -- Value of IMU integral between MUMINUS and MUPLUS. Computed if FIRSTPT=.TRUE., input otherwise.
!     ROUTINES CALLED: SNCNDN, ZROOTS, CALCIMUSYM, CALCIMUSYMF, CALCIMUASYM, CALCIMUASYMF, ELLCUBICREAL, 
!                       ELLCUBICCOMPLEX, ELLQUARTICREAL, ELLQUARTICCOMPLEX, ELLDOUBLECOMPLEX
!                    
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
      integer i,nreal,p(5)
      double precision a,aa,a1,a2,a3,cn,dn,dis,rr,i1mu,i3mu,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
            f,half,iu,qs,h1,h2,f1,f2,g1,g2,l,l2,m1,mneg,mpos,mu0,muf,muplus,uarg, &
            one,pi,pi2,q2,ql2,r,rf,s1,sn,sm,su,theta,third,two,bb,farg,sarg,i2mu, &
            u0,uf,u1,u2,u3,u4,g,cc,dd,ee,iu0,iu1,ellcubicreal,ellcubiccomplex,ellquarticreal, &
            ellquarticcomplex,elldoublecomplex,asech,muminus,iarg,qq,rb2,uplus,yy,realroot(4)
      integer tpm,tpr,ncase
      logical pht,firstpt
      complex*16 c(5),root(4),coefs(7),hroots(6)
      parameter ( one=1.D0, two=2.D0, half=0.5D0, third = 0.3333333333333333D0 )
      pi=acos(-one)
      pi2=two*pi
      uplus=one/(one+sqrt(one-a*a))
      l2=l*l
      ql2=q2+l2
!  Eq. (13): Coefficients of quartic in U(u)
      cc=a*a-q2-l2
      dd=two*((a-l)**2+q2)
      ee=-a*a*q2
!  Determine if U is cubic or quartic and find roots.
      if((ee.eq.0.d0) .and. (dd.ne.0.d0)) then
        p(1)=-1
        p(2)=-1
        p(3)=-1
        p(4)=0
        p(5)=0
        qq=cc*cc/dd/dd/9.d0
        rr=(two*cc**3/dd**3+27.d0/dd)/54.d0
        dis=rr*rr-qq**3
        if(dis.lt.-1.d-16) then
! These are the cubic real roots cases with u1<0<u2<=u3
          theta=acos(rr/qq**1.5d0)
          u1=-two*sqrt(qq)*cos(theta/3.d0)-cc/dd/3.d0
          u2=-two*sqrt(qq)*cos((theta-two*pi)/3.d0)-cc/dd/3.d0
          u3=-two*sqrt(qq)*cos((theta+two*pi)/3.d0)-cc/dd/3.d0
          iu1=0.d0
80        continue
          if(u0.le.u2) then
            if(uf.gt.u2) uf=u2
            ncase=1
! Table 1 Row 1
            if(firstpt.and.(u0.ne.u2)) then
              iu0=ellcubicreal(p,-u1,one,u2,-one,u3,-one,0.d0,0.d0,rffu0,u0,u2)
            elseif(u0.eq.u2) then
              iu0=0.d0
            endif
            if(uf.ne.u2) iu1=ellcubicreal(p,-u1,one,u2,-one,u3,-one,0.d0,0.d0,rffu1,uf,u2)
            iu=su*(iu0-(-one)**tpr*iu1)/sqrt(dd)
          elseif(u0.ge.u3) then
            if(uf.lt.u3) uf=u3
            ncase=2
! Table 1 Row 2
            if(firstpt.and.(u0.ne.u3)) then
              iu0=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,0.d0,0.d0,rffu0,u3,u0)
            elseif(u0.eq.u3) then
              iu0=0.d0
            endif
            if(uf.ne.u3) iu1=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,0.d0,0.d0,rffu1,u3,uf)
            iu=su*(iu0-(-one)**tpr*iu1)/sqrt(dd)
          else
! If uf is in the forbidden region u2 < uf < u3, issue warning and modify it.
            write(6,*) 'WARNING - Unphysical Cubic Real. Input modified.'
            if(su.eq.1) then
              u0=u3
            else
              u0=u2
            endif
! Try again with valid uf.
            goto 80
          endif
        elseif(abs(dis).lt.1.d-16) then
! This is a cubic case with equal roots. We could use Carlson routines here, but it's faster to use elementary functions.
          u1=-two*sqrt(qq)-cc/dd/3.d0
          u2=-two*sqrt(qq)*cos((two*pi)/3.d0)-cc/dd/3.d0
          u3=u2
          tpr=0
          if(u0.le.u2) then
            ncase=1
            if(uf.gt.u2) then 
              uf=u2
              iu=su*1.d300
            else
              farg=(sqrt(uf-u1)+sqrt(u2-u1))/abs(sqrt(uf-u1)-sqrt(u2-u1))
              sarg=(sqrt(u0-u1)+sqrt(u2-u1))/abs(sqrt(u0-u1)-sqrt(u2-u1))
              iu=su*(log(farg)-log(sarg))/sqrt((u2-u1)*dd)    
            endif           
          elseif(u0.ge.u2) then
            ncase=2
            if(uf.lt.u2) then
              uf=u2
              iu=su*1.d300
            else
              farg=(sqrt(uf-u1)+sqrt(u2-u1))/abs(sqrt(uf-u1)-sqrt(u2-u1))
              sarg=(sqrt(u0-u1)+sqrt(u2-u1))/abs(sqrt(u0-u1)-sqrt(u2-u1))
              iu=-su*(log(farg)-log(sarg))/sqrt((u2-u1)*dd)     
            endif
          endif
        else
! This is a cubic complex case with one real root.
          ncase=3
          aa=-sign(1.d0,rr)*(abs(rr)+sqrt(dis))**third
          if(aa.ne.0.d0) then
            bb=qq/aa
          else
            bb=0.d0
          endif
! Table 1 Row 3
          u1=(aa+bb)-cc/dd/3.d0
          f=-one/dd/u1
          g=f/u1
          if(uf.gt.u0) then
            iu=su*ellcubiccomplex(p,-u1,one,0.d0,0.d0,f,g,one,rffu0,u0,uf)/sqrt(dd)
          elseif(uf.lt.u0) then
            iu=-su*ellcubiccomplex(p,-u1,one,0.d0,0.d0,f,g,one,rffu0,uf,u0)/sqrt(dd)
          else
            iu=0.d0         
          endif 
        endif
        if(q2.eq.0.d0) then
! Find muf in the special case q2=0. In this case there can only be 0 or 1 mu turning points.
          s1=sign(1.d0,mu0)
          if(l2.lt.a*a.and.mu0.ne.0.d0) then
            muplus=s1*sqrt(one-l2/a/a)
! Eq. (43)
            muf=muplus/cosh(abs(a*muplus)*iu-s1*sm*asech(mu0/muplus))
          else
            muf=0.d0
          endif
        elseif(a.eq.0.d0) then
! Find muf in the special case a=0
          a1=sm
          muplus=sqrt(q2/ql2)
          if(mu0.gt.muplus) muplus=mu0
          i1mu=acos(mu0/muplus)/sqrt(ql2)
          i3mu=pi/sqrt(ql2)
          if(sm.eq.1) then
! Eq. (30)
            tpm=int((iu-i1mu)/i3mu)+int((1+sign(1.d0,(iu-i1mu)/i3mu))/2.d0)
          else
! Eq. (31)
            tpm=int((iu+i1mu)/i3mu)
          endif
          a2=sm*(-one)**tpm
! Eq. (35)
          a3=two*int((two*dble(tpm)+3.d0-sm)/4.d0)-one
! Eq. (44) using muminus=-muplus
          muf=-muplus*cos(sqrt(ql2)*(iu-a1*i1mu-a3*i3mu)/a2)
        endif
      elseif(ee.eq.0.d0 .and. dd.eq.0.d0) then
! This is the special case where q2=0 and l=a. In this case, U(u)=1.
        ncase=4
        tpr=0
        iu=su*(uf-u0)
        muf=0.d0
      else
        p(1)=-1
        p(2)=-1
        p(3)=-1
        p(4)=-1
        p(5)=0
! These are the quartic cases. First we find the roots.
        if(firstpt) then
          c(1)=dcmplx(one,0.d0)
          c(2)=dcmplx(0.d0,0.d0)
          c(3)=dcmplx(cc,0.d0)
          c(4)=dcmplx(dd,0.d0)
          c(5)=dcmplx(ee,0.d0)
          call zroots(c,4,root,.true.)
          nreal=0
          do i=1,4
            if(dimag(root(i)).eq.0.d0) nreal=nreal+1
          enddo
          if(nreal.eq.2) then
! This is the quartic complex case with 2 real roots.
            ncase=5
            u1=dble(root(1))
            if(dimag(root(2)).eq.0.d0) then
              u4=dble(root(2))
            else
              u4=dble(root(4))
            endif
          elseif(nreal.eq.0) then
            ncase=6
          else
            u1=dble(root(1))
            u2=dble(root(2))
            u3=dble(root(3))
            u4=dble(root(4))
90          continue
! There are a few cases where all roots are real but unphysical.
            if(u2.gt.uplus.and.u3.gt.uplus) then
              ncase=5
            elseif(u0.le.u2) then
              ncase=7
            elseif(u0.ge.u3) then
              ncase=8
            else
! If u2 < u0 < u3, issue warning and modify it.
              write(6,*) 'WARNING--Unphysical Quartic Real. Inputs modified.'
              if(su.eq.1) then
                u0=0.d0
              else
                u0=uplus
              endif
              goto 90
            endif             
          endif
        endif
        if(ncase.eq.5) then
! Cases with one pair of complex roots
! Table 1 Row 5
          qs=sign(1.d0,q2)
          f=-qs*one/abs(ee)/u1/u4
          g=(u4+u1)/u1/u4*f
          if(uf.gt.u0) then
            iu=su*ellquarticcomplex(p,-u1,one,qs*u4,-qs*one,0.d0,0.d0,f,g,one,rffu0,u0,uf)/sqrt(abs(ee))
          elseif(uf.lt.u0) then
            iu=-su*ellquarticcomplex(p,-u1,one,qs*u4,-qs*one,0.d0,0.d0,f,g,one,rffu0,uf,u0)/sqrt(abs(ee))
          else
            iu=0.d0
          endif
        elseif(ncase.eq.6) then
! This is the quartic complex case with no real roots.
! Table 1 Row 6
          ncase=6
! Solve for h1 from Eq. (21):
          coefs(1)=dcmplx(one,0.d0)
          coefs(2)=dcmplx(-cc/sqrt(ee),0.d0)
          coefs(3)=dcmplx(-one,0.d0)
          coefs(4)=dcmplx(sqrt(ee)*(two*cc/ee-(dd/ee)**2),0.d0)
          coefs(5)=dcmplx(-one,0.d0)
          coefs(6)=dcmplx(-cc/sqrt(ee),0.d0)
          coefs(7)=dcmplx(one,0.d0)
          call zroots(coefs,6,hroots,.true.)
          i=0
          h1=0.d0
10        continue
            i=i+1
            if(dimag(hroots(i)).eq.0.d0) h1=dble(hroots(i))
          if(h1.eq.0.d0) goto 10
! Given h1, we can solve for the other coefficients:
          h2=one/h1
          g1=dd/ee/(h2-h1)
          g2=-g1
          f1=one/sqrt(ee)
          f2=f1
          if(uf.gt.u0) then
            iu=su*elldoublecomplex(p,f1,g1,h1,f2,g2,h2,0.d0,0.d0,rffu0,u0,uf)/sqrt(abs(ee))
          elseif(uf.lt.u0) then
            iu=-su*elldoublecomplex(p,f1,g1,h1,f2,g2,h2,0.d0,0.d0,rffu0,uf,u0)/sqrt(abs(ee))
          else
            iu=0.d0
          endif
        else
! These are the quartic real roots cases
          if(abs(u3-u2).gt.1.d-12) then
            iu1=0.d0
            if(ncase.eq.7) then
! Table 1 Row 7
              if(uf.gt.u2) uf=u2
              if(firstpt.and.(u0.ne.u2)) then 
                iu0=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,0.d0,rffu0,u0,u2)
              elseif(u0.eq.u2) then
                iu0=0.d0
              endif
              if(uf.ne.u2) iu1=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,0.d0,rffu1,uf,u2)
              iu=su*(iu0-(-one)**tpr*iu1)/sqrt(abs(ee))
            elseif(ncase.eq.8) then
! Table 1 Row 8
              if(uf.lt.u3) uf=u3
              if(firstpt.and.(u0.ne.u3)) then 
                iu0=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,0.d0,rffu0,u3,u0)
              elseif(u0.eq.u3) then
                iu0=0.d0
              endif
              if(uf.ne.u3) iu1=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,0.d0,rffu1,u3,uf)
              iu=su*(iu0-(-one)**tpr*iu1)/sqrt(abs(ee))
            endif
          else
! These are the equal roots quartic cases.
            tpr=0
            if(ncase.eq.7) then
              if(uf.gt.u2) uf=u2
              iu0=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,0.d0,rffu0,u0,uf)
              iu=su*iu0/sqrt(abs(ee))
            elseif(ncase.eq.8) then
              if(uf.lt.u3) uf=u3
              iu0=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,0.d0,rffu0,u0,uf)
              iu=su*iu0/sqrt(abs(ee))
            else
              write(6,*) 'ERROR - Unphysical Quartic Real'
              ncase=0
            endif
          endif            
        endif
! Find roots of M(mu) (Eq. (12)).
        yy=-0.5d0*(a*a-ql2+sign(one,a*a-ql2)*sqrt((a*a-ql2)**2+4.d0*q2*a*a))
        if((a*a-ql2).lt.0.d0) then
          mneg=-yy/a/a
          mpos=q2/yy
        else
          mneg=q2/yy
          mpos=-yy/a/a
        endif
        if(mpos.gt.1.d0) mpos=1.d0
        muplus=sqrt(mpos)
        if(mneg.lt.0.d0) then
! This is the symmetric roots case, where the orbit can cross the equatorial plane.
          if(muplus.lt.mu0) then
            muplus=mu0
            mpos=muplus*muplus
          endif
          muminus=-muplus
          if(firstpt) call calcimusym(a,mneg,mpos,mu0,muplus,i1mu,i3mu,rffmu1,rffmu3)
          a1=sm
          if(sm.eq.1) then
            tpm=int((iu-i1mu)/i3mu)+int((1+sign(1.d0,(iu-i1mu)/i3mu))/2.d0)
          else
            tpm=int((iu+i1mu)/i3mu)
          endif
          a2=sm*(-one)**tpm
! Eq. (35)
          a3=two*int((two*dble(tpm)+3.d0-sm)/4.d0)-one
! Eq. (42)
          iarg=abs(a)/a2*(iu-a1*i1mu-a3*i3mu)
          uarg=sqrt(mpos-mneg)*iarg
          m1=-mneg/(mpos-mneg)
          call sncndn(uarg,m1,sn,cn,dn)
          muf=muminus*cn
          if(pht) call calcimusymf(a,mneg,mpos,muf,muplus,i2mu,i3mu,rffmu2)
        else
          muplus=sign(1.d0,mu0)*muplus
          muminus=sign(1.d0,mu0)*sqrt(mneg)
          if(abs(muminus).gt.abs(mu0)) then
            muminus=mu0
            mneg=muminus*muminus
          endif
          if(abs(mu0).gt.abs(muplus)) then
            muplus=mu0
            mpos=muplus*muplus
          endif
!          write(6,*) 'abs: ',abs(mu0)-abs(muplus),mu0-muplus,mneg,mpos
          if(firstpt) call calcimuasym(a,mneg,mpos,mu0,muplus,i1mu,i3mu,rffmu1,rffmu3)
          a1=sm
          if(sm.eq.1) then
! Eq. (30)
            tpm=int((iu-i1mu)/i3mu)+int(abs((1+sign(1.d0,(iu-i1mu)/i3mu))/2.d0))
          else
! Eq. (33)
            tpm=int((iu+i1mu)/i3mu)
          endif
          a2=sm*(-one)**tpm
! Eq. (35)
          a3=two*int((two*dble(tpm)+3.d0-sm)/4.d0)-one
! Eq. (39)
          iarg=abs(a)/a2*(iu-a1*i1mu-a3*i3mu)
          uarg=abs(muplus)*iarg
          m1=mneg/mpos
          call sncndn(uarg,m1,sn,cn,dn)
          muf=muminus/dn
          if(pht) call calcimuasymf(a,mneg,mpos,muf,muplus,i2mu,i3mu,rffmu2)
        endif                
      endif
      return
      end

      double precision function ellcubicreal(p,a1,b1,a2,b2,a3,b3,a4,b4,rff,y,x)
!******************************************************************************
!     PURPOSE: Computes \int_y^x dt \Pi_{i=1}^4 (a_i+b_i t)^{p_i/2} for Table 1 Rows 1,2.
!     INPUTS:  Arguments for above integral.
!     OUTPUTS:  Value of integral, and RFF is the RF piece.
!     ROUTINES CALLED: RF,RJ,RC,RD
!     ACCURACY:   Machine.
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
      double precision one,half,two,three,ellcubic,d12,d13,d14,d24,d34,x1,x2,x3,x4, &
                       y1,y2,y3,y4,u1c,u32,u22,w22,u12,q22,p22,i1c,i3c,r12,r13,r24i,r34i, &
                       i2c,k2c,a1,b1,a2,b2,a3,b3,a4,b4,y,x,rc,rd,rj,rf,rff
      integer p(5)
      parameter ( one=1.D0, two=2.D0, half=0.5d0, three=3.D0 )
      ellcubic=0.d0
! (2.1) Carlson (1989)
      d12=a1*b2-a2*b1
      d13=a1*b3-a3*b1
      d14=a1*b4-a4*b1
      d24=a2*b4-a4*b2
      d34=a3*b4-a4*b3
! (2.2) Carlson (1989)
      x1=sqrt(a1+b1*x)
      x2=sqrt(a2+b2*x)
      x3=sqrt(a3+b3*x)
      x4=sqrt(a4+b4*x)
      y1=sqrt(a1+b1*y)
      y2=sqrt(a2+b2*y)
      y3=sqrt(a3+b3*y)
      y4=sqrt(a4+b4*y)
! (2.3) Carlson (1989)
      u1c=(x1*y2*y3+y1*x2*x3)/(x-y)
      u12=u1c**2
      u22=((x2*y1*y3+y2*x1*x3)/(x-y))**2
      u32=((x3*y1*y2+y3*x1*x2)/(x-y))**2
! (2.4) Carlson (1989)
      w22=u12
      w22=u12-b4*d12*d13/d14
! (2.5) Carlson (1989)
      q22=(x4*y4/x1/y1)**2*w22 
      p22=q22+b4*d24*d34/d14
! Now, compute the three integrals we need [-1,-1,-1],[-1,-1,-1,-2], and 
!  [-1,-1,-1,-4]:
      if(p(4).eq.0) then
! (2.21) Carlson (1989)
        rff=rf(u32,u22,u12)
        ellcubic=two*rff
      else
! (2.12) Carlson (1989)
        i1c=two*rff
        if(p(4).eq.-2) then
! (2.14) Carlson (1989)
          i3c=two*rc(p22,q22)-two*b1*d12*d13/three/d14*rj(u32,u22,u12,w22)
! (2.49) Carlson (1989)
          ellcubic=(b4*i3c-b1*i1c)/d14
        else
! (2.1)  Carlson (1989)
          r12=a1/b1-a2/b2
          r13=a1/b1-a3/b3
          r24i=b2*b4/(a2*b4-a4*b2)
          r34i=b3*b4/(a3*b4-a4*b3)
! (2.13) Carlson (1989)
          i2c=two/three*d12*d13*rd(u32,u22,u12)+two*x1*y1/u1c
! (2.59) & (2.6) Carlson (1989)
          k2c=b2*b3*i2c-two*b4*(x1*x2*x3/x4**2-y1*y2*y3/y4**2)
! (2.62) Carlson (1989)
          ellcubic=half*b4/d14/d24/d34*k2c &
           +(b1/d14)**2*(one-half*r12*r13*r24i*r34i)*i1c
        endif
      endif
      ellcubicreal=ellcubic
      return
      end

      double precision function ellcubiccomplex(p,a1,b1,a4,b4,f,g,h,rff,y,x)
!******************************************************************************
!     PURPOSE: Computes \int_y^x dt \Pi_{i=1,4} (a_i+b_i t)^{p_i/2} (f+gt+ht^2)^{p_2/2} for
!              Table 1 Row 3.
!     INPUTS:  Arguments for above integral.
!     OUTPUTS:  Value of integral, and RFF is the RF piece.
!     ROUTINES CALLED: RF,RJ,RC,RD
!     ACCURACY:   Machine.
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      double precision a1,b1,a4,b4,f,g,h,y,x,x1,x4,y1,y4,d14,beta1,beta4,a11,c44, &
                       a142,xi,eta,m2,lp2,lm2,i1c,u,u2,wp2,w2,q2,p2,rho,i3c,r24xr34,r12xr13, &
                       n2c,k2c,ellcubic,one,two,four,three,half,six,rc,rd,rj,rf,rff
      integer p(5)
      parameter ( one=1.D0, two=2.D0, half=0.5d0, three=3.D0, four=4.d0, six=6.d0 )
      ellcubic=0.d0
      x1=sqrt(a1+b1*x)
      x4=sqrt(a4+b4*x)
      y1=sqrt(a1+b1*y)
      y4=sqrt(a4+b4*y)
      d14=a1*b4-a4*b1
! (2.2) Carlson (1991)
      beta1=g*b1-two*h*a1
      beta4=g*b4-two*h*a4
! (2.3) Carlson (1991)
      a11=sqrt(two*f*b1*b1-two*g*a1*b1+two*h*a1*a1)
      c44=sqrt(two*f*b4*b4-two*g*a4*b4+two*h*a4*a4)
      a142=two*f*b1*b4-g*(a1*b4+a4*b1)+two*h*a1*a4
! (2.4) Carlson (1991)
      xi=sqrt(f+g*x+h*x*x)
      eta=sqrt(f+g*y+h*y*y)
! (3.1) Carlson (1991):
      m2=((x1+y1)*sqrt((xi+eta)**two-h*(x-y)**two)/(x-y))**two
! (3.2) Carlson (1991):
      lp2=m2-beta1+sqrt(two*h)*a11
      lm2=m2-beta1-sqrt(two*h)*a11
      if(p(4).eq.0) then
        rff=rf(m2,lm2,lp2)
! (1.2)   Carlson (1991)
        ellcubic=four*rff
      else
! (3.8)  1991
        i1c=four*rff
! (3.3) 1991
        u=(x1*eta+y1*xi)/(x-y)
        u2=u*u
        wp2=m2-b1*(a142+a11*c44)/d14
        w2=u2-a11**two*b4/two/d14
! (3.4) 1991
        q2=(x4*y4/x1/y1)**two*w2
        p2=q2+c44**two*b4/two/d14
! (3.5) 1991
        rho=sqrt(two*h)*a11-beta1
! (3.9) 1991
        if(p(4).eq.-2) then
! (2.49) Carlson (1989)
          i3c=(two*a11/three/c44)*((-four*b1/d14)*(a142+a11*c44) &
          *rj(m2,lm2,lp2,wp2)-six*rff+three*rc(u2,w2)) &
          +two*rc(p2,q2)
          ellcubic=(b4*i3c-b1*i1c)/d14
        else
! (2.19) Carlson (1991)
          r24xr34=half*c44**two/h/b4**two
          r12xr13=half*a11**two/h/b1**two
! (3.11) Carlson (1991)
          n2c=two/three*sqrt(two*h)/a11*(four*rho*rd(m2,lm2,lp2) &
            -six*rff+three/u)+two/x1/y1/u
! (2.5) & (3.12) Carlson (1991)
          k2c=half*a11**two*n2c-two*d14*(xi/x1/x4**two-eta/y1/y4**two)
! (2.62) Carlson (1989)
          ellcubic=half/d14/(h*b4*r24xr34)*k2c+(b1/d14)**two*(one-half*r12xr13/r24xr34)*i1c
        endif
      endif
      ellcubiccomplex=ellcubic
      return
      end

      double precision function asech(x)
!******************************************************************************
!     PURPOSE: Computes asech(x)
!     INPUTS:  x>0.
!     OUTPUTS:  asech(x)
!     ROUTINES CALLED: *
!     ACCURACY:   Machine.
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
      double precision x
      if(x.gt.0.d0) then
        asech=log((1.d0+sqrt(1.d0-x*x))/x)
      else
        asech=0.d0
      endif
      return
      end

      double precision function ellquarticreal(p,a1,b1,a2,b2,a3,b3,a4,b4,a5,b5,rff,y,x)
!******************************************************************************
!     PURPOSE: Computes \int_y^x dt \Pi_{i=1}^5 (a_i+b_i t)^{p_i/2} for Table 1 Rows 7,8.
!     INPUTS:  Arguments for above integral.
!     OUTPUTS:  Value of integral, and RFF is the RF piece.
!     ROUTINES CALLED: RF,RJ,RC,RD
!     ACCURACY:   Machine.    
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
      double precision a1,b1,a2,b2,a3,b3,a4,b4,a5,b5,y,x,rff, &
                       d12,d13,d14,d24,d34,d15,d25,d35,d45,x1,x2,x3,x4,y1,y2,y3,y4,a111m1, &
                       u122,u132,u142,i1,x52,y52,w22,q22,p22,i3,i2,r12,r13, &
                       r25i,r35i,a111m1m2,w12,q12,p12,i3p,ellquartic,one,half,two,three, &
                       rc,rd,rj,rf
      integer p(5)
      parameter ( one=1.D0, two=2.D0, half=0.5d0, three=3.D0 )
! (2.1) Carlson (1988)
      d12=a1*b2-a2*b1
      d13=a1*b3-a3*b1
      d14=a1*b4-a4*b1
      d24=a2*b4-a4*b2
      d34=a3*b4-a4*b3
      d15=a1*b5-a5*b1
      d25=a2*b5-a5*b2
      d35=a3*b5-a5*b3
      d45=a4*b5-a5*b4
! (2.2) Carlson (1988)
      x1=sqrt(a1+b1*x)
      x2=sqrt(a2+b2*x)
      x3=sqrt(a3+b3*x)
      x4=sqrt(a4+b4*x) 
      y1=sqrt(a1+b1*y)
      y2=sqrt(a2+b2*y)
      y3=sqrt(a3+b3*y)
      y4=sqrt(a4+b4*y)
! (2.3) Carlson (1988)
      u122=((x1*x2*y3*y4+y1*y2*x3*x4)/(x-y))**two
      u132=((x1*x3*y2*y4+y1*y3*x2*x4)/(x-y))**two
      u142=((x1*x4*y2*y3+y1*y4*x2*x3)/(x-y))**two
! Now, compute the three integrals we need [-1,-1,-1,-1],[-1,-1,-1,-1,-2], 
!  [-1,-1,-1,-1,-4]:
      if(p(5).eq.0) then
        rff=rf(u122,u132,u142)
! (2.17) Carlson (1988)
        ellquarticreal=two*rff
        return
      else
! (2.13) Carlson (1988)
        i1=two*rff
        x52=a5+b5*x
        y52=a5+b5*y
! (2.4) Carlson (1988)
        w22=u122-d13*d14*d25/d15
! (2.5) Carlson (1989)
        q22=x52*y52/(x1*y1)**two*w22
        p22=q22+d25*d35*d45/d15
! (2.15) Carlson (1988)
        if(p(5).eq.-2) then
! (2.35) Carlson (1988)
          i3=two*d12*d13*d14/three/d15*rj(u122,u132,u142,w22)+two*rc(p22,q22)
          ellquarticreal=(b5*i3-b1*i1)/d15
          return
        else
          i2=two/three*d12*d13*rd(u122,u132,u142)+two*x1*y1/x4/y4/sqrt(u142)
! (2.1)  Carlson (1988)
          r12=a1/b1-a2/b2
          r13=a1/b1-a3/b3
          r25i=b2*b5/(a2*b5-a5*b2)
          r35i=b3*b5/(a3*b5-a5*b3)
! (2.48) Carlson (1988)
          a111m1m2=x1*x2*x3/x4/x52-y1*y2*y3/y4/y52
          ellquarticreal=half*b5**two*d24*d34/d15/d25/d35/d45*i2 &
         + (b1/d15)**two*(one-half*r12*r13*r25i*r35i)*i1-b5**two/d15/d25/d35*a111m1m2
          return
        endif
      endif
      ellquarticreal=0.d0
      return
      end

      double precision function ellquarticcomplex(p,a1,b1,a4,b4,a5,b5,f,g,h,rff,y,x)
!******************************************************************************
!     PURPOSE: Computes \int_y^x dt \Pi_{i=1,4,5} (a_i+b_i t)^{p_i/2} (f+gt+ht^2)^{p_2/2} 
!              for Table 1 Row 5.
!     INPUTS:  Arguments for above integral.
!     OUTPUTS:  Value of integral, and RFF is the RF piece.
!     ROUTINES CALLED: RF,RJ,RC,RD
!     ACCURACY:   Machine.    
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
! This routine computes Carlson elliptic integrals for Table 1, Row 5 using
! Carlson (1991)
! JAD 2/20/2009
      double precision a1,b1,a4,b4,a5,b5,f,g,h,y,x,one,half,two,three,four,six, &
                       x1,x4,y1,y4,d14,a11,c44,a142,xi,eta,m2,lp2,lm2,i1,ellquartic, &
                       d15,x52,y52,c552,c55,a152,u,u2,wp2,w2,q2,p2,i3,i2,a111m1m2,rho, &
                       d45,rc,rd,rj,rf,rff
      integer p(5)
      parameter ( one=1.D0, two=2.D0, half=0.5d0, three=3.D0, four= 4.D0, &
                  six=6.d0 )
! (2.1) Carlson (1991)
      x1=sqrt(a1+b1*x)
      x4=sqrt(a4+b4*x)
      y1=sqrt(a1+b1*y)
      y4=sqrt(a4+b4*y)
! (2.3) Carlson (1991)
      a11=sqrt(two*f*b1*b1-two*g*a1*b1+two*h*a1*a1)
      c44=sqrt(two*f*b4*b4-two*g*a4*b4+two*h*a4*a4)
      a142=two*f*b1*b4-g*(a1*b4+a4*b1)+two*h*a1*a4
! (2.4) Carlson (1991)
      xi= sqrt(f+g*x+h*x*x)
      eta=sqrt(f+g*y+h*y*y)
! (2.6) Carlson (1991):
      m2=((x1*y4+y1*x4)*sqrt((xi+eta)**two-h*(x-y)**two)/(x-y))**two
! (2.7) Carlson (1991):
      lp2=m2+a142+a11*c44
      lm2=max(m2+a142-a11*c44,0.d0)
      if(p(5).eq.0.d0) then
! (1.2) Carlson (1991)
        rff=rf(m2,lm2,lp2)
        ellquarticcomplex=four*rff
        return
      else
! (2.14)  1991
        i1=four*rff
! (2.1) 1991
        d14=a1*b4-a4*b1
        d15=a1*b5-a5*b1
        d45=a4*b5-a5*b4
        x52=a5+b5*x
        y52=a5+b5*y
! (2.3) 1991
        c552=two*f*b5*b5-two*g*a5*b5+two*h*a5*a5
        c55=sqrt(c552)
        a152=two*f*b1*b5-g*(a1*b5+a5*b1)+two*h*a5*a1
! (2.8) 1991
        u=(x1*x4*eta+y1*y4*xi)/(x-y)
        u2=u*u
        wp2=m2+d14*(a152+a11*c55)/d15
        w2=u2-a11**two*d45/two/d15
! (2.9) 1991
        q2=x52*y52/(x1*y1)**two*w2
        p2=q2+c55**two*d45/two/d15
! (2.15) 1991
        if(p(5).eq.-2) then
! (2.35) Carlson (1988)
          i3=(two*a11/three/c55)*((four*d14/d15)*(a152+a11*c55)*rj(m2,lm2,lp2,wp2) &
              -six*rff+three*rc(u2,w2))+two*rc(p2,q2)
          ellquarticcomplex=(b5*i3-b1*i1)/d15
          return
        else
! (2.15) Carlson (1991)
          i2=two*a11/three/c44*(four*(a142+a11*c44)*rd(m2,lm2,lp2)-six*rff) &
               +two*a11/c44/u+two*x1*y1/x4/y4/u
! (2.5)  Carlson (1991)
          a111m1m2=x1*xi/x4/x52-y1*eta/y4/y52
          ellquarticcomplex=b5**two/(two*d15*d45)*c44**two/c552*i2+ &
                          b1**two/d15**two*(one-half*b5**two*a11**two/b1**two/c552)*i1-two*b5**two/d15/c552*a111m1m2          
          return
          endif 
        endif
      ellquarticcomplex=0.d0
      return
      end

      double precision function elldoublecomplex(p,f1,g1,h1,f2,g2,h2,a5,b5,rff,y,x)
!******************************************************************************
!     PURPOSE: Computes \int_y^x dt (f_1+g_1t+h_1t^2)^{p_1/2} (f_2+g_2t+h_2t^2)^{p_2/2} (a_5+b_5t)^{p_5/2}
!              for Table 1 Row 6.
!     INPUTS:  Arguments for above integral.
!     OUTPUTS:  Value of integral, and RFF is the RF piece.
!     ROUTINES CALLED: RF,RJ,RC,RD
!     ACCURACY:   Machine.    
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
! This routine computes Carlson elliptic integrals for Table 1, Row 6
! using Carlson (1992).
! JAD 2/20/2009
      double precision one,half,two,three,four,six,f1,g1,h1,f2,g2,h2,a5,b5,y,x,xi1,xi2,eta1,eta2, &
                       theta1,theta2,zeta1,zeta2,m,m2,delta122,delta112,delta222,delta,deltap,lp2,lm2, &
                       deltam,rff,ellquartic,u,u2,alpha15,beta15,alpha25,beta25,lambda,omega2,psi,xi5,eta5, &
                       gamma1,gamma2,am111m1,a1111m4,xx,s,mu,t,v2,b2,a2,h,a1111m2,xi1p,b,g,sigma,lambda0, &
                       omega02,psi0,x0,mu0,b02,a02,h0,s2,t2,eta1p,t02,v02,psi2,t0,rc,rd,rj,rf
      integer p(5)
      one=1.d0
      half=0.5d0
      two=2.d0
      three=3.d0
      four=4.d0
      six=6.d0
! (2.1) Carlson (1992)
      xi1=sqrt(f1+g1*x+h1*x**two)
      xi2=sqrt(f2+g2*x+h2*x**two)
      eta1=sqrt(f1+g1*y+h1*y*y)
      eta2=sqrt(f2+g2*y+h2*y*y)
! (2.4) Carlson (1992)
      theta1=two*f1+g1*(x+y)+two*h1*x*y
      theta2=two*f2+g2*(x+y)+two*h2*x*y
! (2.5) Carlson (1992)
      zeta1=sqrt(2*xi1*eta1+theta1)
      zeta2=sqrt(2*xi2*eta2+theta2)
! (2.6) Carlson (1992)
      m=zeta1*zeta2/(x-y)
      m2=m*m
! (2.7) Carlson (1992)
      delta122=two*f1*h2+two*f2*h1-g1*g2
      delta112=four*f1*h1-g1*g1
      delta222=four*f2*h2-g2*g2
      delta=sqrt(delta122*delta122-delta112*delta222)
! (2.8) Carlson (1992)
      deltap=delta122+delta
      deltam=delta122-delta
      lp2=m2+deltap
      lm2=m2+deltam
      if(p(5).eq.0) then
! (2.36) Carlson (1992)
        rff=rf(m2,lm2,lp2)
        ellquartic=four*rff
      else
! (2.6) Carlson (1992)
        u=(xi1*eta2+eta1*xi2)/(x-y)
        u2=u*u
! (2.11) Carlson (1992)
        alpha15=two*f1*b5-g1*a5 
        alpha25=two*f2*b5-g2*a5
        beta15=g1*b5-two*h1*a5 
        beta25=g2*b5-two*h2*a5
! (2.12) Carlson (1992)
        gamma1=half*(alpha15*b5-beta15*a5)
        gamma2=half*(alpha25*b5-beta25*a5)
! (2.13) Carlson (1992)
        lambda=delta112*gamma2/gamma1
        omega2=m2+lambda
        psi=half*(alpha15*beta25-alpha25*beta15)
        psi2=psi*psi
! (2.15) Carlson (1992)
        xi5=a5+b5*x
        eta5=a5+b5*y
! (2.16) Carlson (1992)
        am111m1=one/xi1*xi2-one/eta1*eta2
        a1111m4=xi1*xi2/xi5**two-eta1*eta2/eta5**two
! (2.17) Carlson (1992)
        xx = xi5*eta5*(theta1*half*am111m1-xi5*eta5*a1111m4)/(x-y)**two
! (2.18) Carlson (1992)
        s=half*(m2+delta122)-u2
        s2=s*s
! (2.19) Carlson (1992)
        mu=gamma1*xi5*eta5/xi1/eta1
        t=mu*s+two*gamma1*gamma2
        t2=t*t
        v2=mu**two*(s2+lambda*u2)
! (2.20) Carlson (1992)
        b2=omega2**two*(s2/u2+lambda)
        a2=b2+lambda**two*psi2/gamma1/gamma2
! (2.22) Carlson (1992)
        h=delta112*psi*(rj(m2,lm2,lp2,omega2)/three+half*rc(a2,b2))/gamma1**two-xx*rc(t2,v2)
        if (p(5).eq.-2) then
! (2.39) Carlson (1992)
          ellquartic=-two*(b5*h+beta15*rff/gamma1)
        else
          a1111m2=xi1*xi2/xi5-eta1*eta2/eta5
! (2.2) Carlson (1992)
          xi1p=half*(g1+two*h1*x)/xi1
          eta1p=half*(g1+two*h1*y)/eta1
! (2.3) Carlson (1992)
          b=xi1p*xi2-eta1p*eta2
! (2.9) Carlson (1992)
          g=two/three*delta*deltap*rd(m2,lm2,lp2)+half*delta/u+(delta122*theta1-delta112*theta2)/four/xi1/eta1/u
! (2.10) Carlson (1992)  
          sigma=g-deltap*rff+b
! (2.41) Carlson (1992)
          ellquartic=b5*(beta15/gamma1+beta25/gamma2)*h+beta15**two*rff/gamma1**two+ &
                       b5**two*(sigma-b5*a1111m2)/gamma1/gamma2
        endif
      endif
      elldoublecomplex=ellquartic
      return
      end
!**********************************************************************************
      subroutine geophitime(u0,uf,mu0,muf,a,l,l2,q2,tpm,tpr,su,sm,iu,h1,phimu,tmu, &
            ncase,u1,u2,u3,u4,phiu,tu,lambda,rffu0,rffu1,rffmu1,rffmu2,rffmu3, &
            rdc,rjc,tu01,tu02,tu03,tu04,tmu1,tmu3,phimu1,phimu3,firstpt)
!******************************************************************************
!     PURPOSE: Computes final polar angle muf at an inverse radius uf given initial inverse radius
!               and polar angle, and geodesic constants of the motion.
!
!     INPUTS:   U0 -- Starting u value. If U0=0, DTI and LAMBDAI values will blow up.
!               UF -- Final u value.
!               MU0 -- Starting mu=cos(theta) value.
!               MUF -- Final mu value.
!               A -- Black hole spin, on interval [0,1).
!               L -- Dimensionless z-component of angular momentum.
!               L2 -- L*L
!               Q2 -- Dimensionless Carter's constant.
!               TPM -- Number of mu turning points between MU0 and MUF.
!               TPR -- Number of u turning points reached between U0 and UF.
!               SU -- Initial du/dlambda, =1 (-1) for ingoing (outgoing) rays.
!               SM -- Initital dmu/dlambda.
!               IU -- Value of IU=IMU
!               H1 -- Value of h1 from Eq. (21) if NCASE=6
!               NCASE -- Case label corresponding to Table 1.
!               U1,2,3,4 -- (Real) roots of U(u) in increasing order.
!               RFFU0 -- Value of RF relevant for U0 piece of IU integral.
!               RFFU1 -- Value of RF relevant for UF piece of IU integral.
!               RFFMU1 -- Value of RF relevant for MU0 piece of IMU integral.
!               RFFMU2 -- Value of RF relevant for MUF piece of IMU integral.
!               RFFMU3 -- Value of RF relevant for turning point piece of IMU integral.
!               FIRSTPT -- Boolean variable. If .TRUE., RDC, RJC, TU01, TU02, TU03, TU04, TMU1, TMU3, PHIMU1 and
!                          PHIMU3 are computed. Otherwise, all are input.
!     OUTPUTS:  PHIMU -- Value of MU term from Eq. (15)
!               TMU -- Value of MU term from Eq. (14)
!               PHIU -- Value of U term from Eq. (15)
!               TU -- Value of U term from Eq. (14)
!               LAMBDA -- Affine parameter from Eq. (49)
!               RDC -- Value of RD for the complete elliptic integral of the 2nd kind.
!               RJC -- Value of RJ for the complete elliptic integral of the 3rd kind.
!               TU01 -- Value of the U0 part of the fourth term of Eq. (47). Computed if FIRSTPT=.TRUE., ignored
!                       if no physical turning points are present. Input otherwise.
!               TU02 -- Value of the U0 part of the third term of Eq. (47). Computed if FIRSTPT=.TRUE., ignored
!                       if no physical turning points are present. Input otherwise.
!               TU03 -- Value of the U0 part of the first term of Eq. (47). Computed if FIRSTPT=.TRUE., ignored if
!                       no physical turning points are present. Input otherwise.
!               TU04 -- Value of the U0 part of the second term of Eq. (47). Computed if FIRSTPT=.TRUE., ignored if
!                       no physical turning points are present. Input otherwise.
!     ROUTINES CALLED: ELLCUBICREAL, ELLCUBICCOMPLEX, ELLQUARTICREAL, ELLQUARTICCOMPLEX, ELLDOUBLECOMPLEX,
!                       TFNKERR, PHIFNKERR, CALCPHITMUSYM, CALCPHITMUASYM                   
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
      double precision a,u1,u2,u3,u4,half,l,l2,m1,mneg,mpos,mu0,muf,one,um,uplus,yy, &
            phimu,phimu0,phiu0,phiuf,phiu,pi,pi2,q2,ql2,rf,s1,sm,su,tmu,tmu0,tu0,tuf,tu,two,umi,ee,dd, &
            u0,uf,tmu1,tmu2,tmu3,phimu11,phimu12,phimu21,phimu22,phimu31,phimu32,lambda, &
            tu01,tu11,tu02,tu12,tu03,tu13,tu04,tu14,phiu01,phiu02,phiu11,phiu12,rffu0,rffu1, &
            ellcubicreal,ellcubiccomplex,ellquarticreal,ellquarticcomplex,elldoublecomplex, &
            f1,f2,g1,g2,h1,h2,muminus,muplus,a1,a2,a3,tfnkerr,phifnkerr,qs,ur,f,g,h,tu1,tu2, &
            tu3,tu4,phiu1,phiu2,iu1,iu0,phimu1,phimu2,vfm,vfp,vsm,vsp,phimu3,iu,lambdau,farg, &
            rffmu1,rffmu2,rffmu3,rdc,rjc
      integer tpm,tpr,ncase,p(5)
! firstpt is a boolean variable indicating if we need to calculate integrals involving only the initial and turning points.
      logical firstpt
      parameter ( one=1.d0, two=2.d0, half=0.5d0 )
      pi=acos(-one)
      pi2=two*pi
      p(1)=-1
      p(2)=-1
      p(3)=-1
      uplus=one/(one+sqrt(one-a*a))
! Only calculate 1/u_- since u_- blows up when a=0.
      umi=one-sqrt(one-a*a)
      ur=-one/(two*sqrt(one-a*a))
      qs=sign(1.d0,q2)
      dd=two*((a-l)**two+q2)
! Eq. (22)
      ee=-a*a*q2
      ql2=q2+l2
      a1=sm
      a2=sm*(-one)**tpm
! Eq. (35)
      a3=two*int((two*dble(tpm)+3.d0-sm)/4.d0)-one
      if(ncase.eq.0) then
        tu=0.d0
        phiu=0.d0
        phimu=0.d0
        tmu=0.d0
        lambdau=0.d0
      elseif(ncase.lt.3) then
! These are the cubic real roots cases with u1<0<u2<=u3
        p(5)=0
        if(abs(u3-u2).lt.1.D-12) then
! These are the equal roots cases
          tu=su*tfnkerr(u0,uf,u1,u2,l,a)/sqrt(dd)
          phiu=su*(phifnkerr(uf,u1,u2,l,a)-phifnkerr(u0,u1,u2,l,a))/sqrt(dd)   
          if(u0.ge.u3) then
            phiu=-phiu
            tu=-tu
          endif       
        elseif(u0.le.u2) then
! Table 1 Row 1
          if(firstpt.and.(u0.ne.u2)) then
            p(4)=-2
! First three u0 integrals in Eq. (47)
            phiu01=ellcubicreal(p,-u1,one,u2,-one,u3,-one,one,-one/uplus,rffu0,u0,u2)
            phiu02=ellcubicreal(p,-u1,one,u2,-one,u3,-one,one,-umi,rffu0,u0,u2)
            tu02=ellcubicreal(p,-u1,one,u2,-one,u3,-one,0.d0,one,rffu0,u0,u2)
            tu03=phiu01
            tu04=phiu02
            p(4)=-4
! Fourth u0 integral in Eq. (47)
            tu01=ellcubicreal(p,-u1,one,u2,-one,u3,-one,0.d0,one,rffu0,u0,u2)
          elseif(u0.eq.u2) then
            tu01=0.d0
            tu02=0.d0
            tu03=0.d0
            tu04=0.d0
            phiu01=0.d0
            phiu02=0.d0
          else
            phiu01=tu03
            phiu02=tu04
          endif
          if(uf.ne.u2) then
            p(4)=-2
! First three uf integrals in Eq. (47)
            phiu11=ellcubicreal(p,-u1,one,u2,-one,u3,-one,one,-one/uplus,rffu1,uf,u2)
            phiu12=ellcubicreal(p,-u1,one,u2,-one,u3,-one,one,-umi,rffu1,uf,u2)
            tu12=ellcubicreal(p,-u1,one,u2,-one,u3,-one,0.d0,one,rffu1,uf,u2)
            tu13=phiu11
            tu14=phiu12
            p(4)=-4
! Fourth uf integral in Eq. (47)
            tu11=ellcubicreal(p,-u1,one,u2,-one,u3,-one,0.d0,one,rffu1,uf,u2)
          else
            tu11=0.d0
            tu12=0.d0
            tu13=0.d0
            tu14=0.d0
            phiu11=0.d0
            phiu12=0.d0
          endif
! Eq. (47) for u0 piece
          tu0=(umi-one/uplus)*tu01+(umi**two-one/uplus**two)*tu02- &
            (2.d0*a*(a-l)+a**two/uplus+one/uplus**3)*tu03+ &
            (2.d0*a*(a-l)+a**two*umi+umi**3)*tu04
! Eq. (47) for uf piece
          tu1=(umi-one/uplus)*tu11+(umi**two-one/uplus**two)*tu12- &
            (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu13+ &
            (2.d0*a*(a-l)+a**2*umi+umi**3)*tu14
! Combine according to Eq. (54)
          tu=su*(tu0-(-one)**tpr*tu1)/sqrt(dd)*ur
! Eq. (48) for u0 piece
          phiu0=-(l/uplus+2.d0*(a-l))*phiu01+(l*umi+2.d0*(a-l))*phiu02
! Eq. (48) for uf piece
          phiu1=-(l/uplus+2.d0*(a-l))*phiu11+(l*umi+2.d0*(a-l))*phiu12
! Combine according to Eq. (54)
          phiu=su*(phiu0-(-one)**tpr*phiu1)/sqrt(dd)*ur
! Eq. (49)
          lambdau=su*(tu01-(-one)**tpr*tu11)/sqrt(dd)
        elseif(u0.ge.u3) then
! Table 1 Row 2
          if(firstpt.and.(u0.ne.u3)) then
            p(4)=-2
! First three u0 integrals in Eq. (47)
            phiu01=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,one,-one/uplus,rffu0,u3,u0)
            phiu02=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,one,-umi,rffu0,u3,u0)
            tu02=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,0.d0,one,rffu0,u3,u0)
            tu03=phiu01
            tu04=phiu02
! Fourth u0 integral in Eq. (47)
            p(4)=-4
            tu01=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,0.d0,one,rffu0,u3,u0)
          elseif(u0.eq.u3) then
            tu01=0.d0
            tu02=0.d0
            tu03=0.d0
            tu04=0.d0
            phiu01=0.d0
            phiu02=0.d0
          else
            phiu01=tu03
            phiu02=tu04
          endif
          if(uf.ne.u3) then
            p(4)=-2
! First three uf integrals in Eq. (47)
            phiu11=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,one,-one/uplus,rffu1,u3,uf)
            phiu12=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,one,-umi,rffu1,u3,uf)
            tu12=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,0.d0,one,rffu1,u3,uf)
            tu13=phiu11
            tu14=phiu12
            p(4)=-4
! Fourth uf integral in Eq. (47)
            tu11=-ellcubicreal(p,-u1,one,-u2,one,-u3,one,0.d0,one,rffu1,u3,uf)
          else
            tu11=0.d0
            tu12=0.d0
            tu13=0.d0
            tu14=0.d0
            phiu11=0.d0
            phiu12=0.d0
          endif
! Eq. (47) for u0 part
          tu0=(umi-one/uplus)*tu01+(umi**2-one/uplus**2)*tu02- &
            (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu03+ &
            (2.d0*a*(a-l)+a**2*umi+umi**3)*tu04
! Eq. (47) for uf part
          tu1=(umi-one/uplus)*tu11+(umi**2-one/uplus**2)*tu12- &
            (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu13+ &
            (2.d0*a*(a-l)+a**2*umi+umi**3)*tu14
! Combine according to Eq. (54)
          tu=su*(tu0-(-one)**tpr*tu1)/sqrt(dd)*ur
! Eq. (48) for u0 part
          phiu0=-(l/uplus+2.d0*(a-l))*phiu01+(l*umi+2.d0*(a-l))*phiu02
! Eq. (48) for uf part
          phiu1=-(l/uplus+2.d0*(a-l))*phiu11+(l*umi+2.d0*(a-l))*phiu12
! Combine according to Eq. (54)
          phiu=su*(phiu0-(-one)**tpr*phiu1)/sqrt(dd)*ur
! Eq. (49)
          lambdau=su*(tu01-(-one)**tpr*tu11)/sqrt(dd)
        endif
      elseif(ncase.eq.3) then
! This is a cubic complex case with one real root.
! Table 1 Row 3
        ncase=3
        f=-one/dd/u1
        g=f/u1
        h=one
        if(u0.lt.uf) then
          p(4)=-4
! Fourth integral in Eq. (47)
          tu1=ellcubiccomplex(p,-u1,one,0.d0,one,f,g,h,rffu0,u0,uf)
          p(4)=-2
! First three integrals in Eq. (47)
          tu2=ellcubiccomplex(p,-u1,one,0.d0,one,f,g,h,rffu0,u0,uf)
          tu3=ellcubiccomplex(p,-u1,one,one,-one/uplus,f,g,h,rffu0,u0,uf)
          tu4=ellcubiccomplex(p,-u1,one,one,-umi,f,g,h,rffu0,u0,uf)
! Eq. (47)
          tu=su*ur/sqrt(dd)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Minus sign in phi components is from flipping the sign of the u/u_\pm-1 factor to keep arguments positive.
          phiu0=-tu3
          phiu1=-tu4
! Eq. (48)
          phiu=su*ur/sqrt(dd)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
          lambdau=su*tu1/sqrt(dd)
        elseif(u0.gt.uf) then
          p(4)=-4
! Fourth integral in Eq. (47)
          tu1=-ellcubiccomplex(p,-u1,one,0.d0,one,f,g,h,rffu0,uf,u0)
          p(4)=-2
! First three integrals in Eq. (47)
          tu2=-ellcubiccomplex(p,-u1,one,0.d0,one,f,g,h,rffu0,uf,u0)
          tu3=-ellcubiccomplex(p,-u1,one,one,-one/uplus,f,g,h,rffu0,uf,u0)
          tu4=-ellcubiccomplex(p,-u1,one,one,-umi,f,g,h,rffu0,uf,u0)
! Eq. (47)
          tu=su*ur/sqrt(dd)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Minus sign in phi components is from flipping the sign of the u/u_\pm-1 factor to keep arguments positive.
          phiu0=-tu3
          phiu1=-tu4
! Eq. (48)
          phiu=su*ur/sqrt(dd)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
! Eq. (49)
          lambdau=su*tu1/sqrt(dd)
        else
          tu=0.d0
          phiu=0.d0
          lambdau=0.d0
        endif
      endif
      if(q2.eq.0.d0) then
! Calculate mu components in the special case q2=0, where the t and phi integrals are elementary.
        s1=sign(1.d0,mu0)
        a1=s1*sm
        a2=s1*sm*(-one)**(tpm+1)
        if(abs(l).lt.abs(a)) then
          muplus=s1*sqrt(one-l2/a/a)
          phimu1=sign(1.d0,muplus*l/a)*(atan((muplus-tan(asin(mu0/muplus)/two))/sqrt(one-muplus**2))+ &
                  atan((muplus+tan(asin(mu0/muplus)/two))/sqrt(one-muplus**2)))
          phimu2=sign(1.d0,muplus*l/a)*(atan((muplus-tan(asin(muf/muplus)/two))/sqrt(one-muplus**2))+ &
                 atan((muplus+tan(asin(muf/muplus)/two))/sqrt(one-muplus**2)))
          phimu=a1*phimu1+a2*phimu2
          tmu=abs(muplus*a)*(a2*sqrt(1.d0-muf**2/muplus**2)+a1*sqrt(1.d0-mu0**2/muplus**2))
        else
          phimu=0.d0
          tmu=0.d0
        endif
      elseif(a.eq.0.d0) then
! Calculate mu components in the special case a=0. The t and phi integrals are again elementary.
        tmu=0.d0
        muplus=sqrt(q2/ql2)
        vfm= (muf-muplus**2)/(one-muf)/muplus
        vfp=-(muf+muplus**2)/(one+muf)/muplus
! This is code to suppress floating point errors in phimu calculation.
        if(abs(vfm).gt.one) vfm=sign(1.d0,vfm)*one
        if(abs(vfp).gt.one) vfp=sign(1.d0,vfp)*one
        if((mu0-muplus**2).eq.0.d0) then
          vsm=-one
        else
          vsm=(mu0-muplus**2)/(one-mu0)/muplus
        endif
        if((mu0+muplus**2).eq.0.d0) then
          vsp=-one
        else
          vsp=-(mu0+muplus**2)/(one+mu0)/muplus
        endif
        if(abs(vsp).gt.one) vsp=sign(1.d0,vsp)*one
        if(abs(vsm).gt.one) vsm=sign(1.d0,vsm)*one
        phimu1=pi-asin(vsm)+asin(vsp)
        phimu2=asin(vfm)-asin(vfp)+pi
        phimu3=two*pi
        phimu=-l*iu+sign(1.d0,l)*0.5d0*(a1*phimu1+a2*phimu2+a3*phimu3)
      endif
      if(ncase.eq.4) then
! This is the special case where q2=0 and l=a. U(u)=1 and the t and phi u components are elementary.
! Eq. (48)
        phiu=su*ur*(l*log((uf/uplus-one)/(u0/uplus-one)) &
             -l*umi*umi*log((uf*umi-one)/(u0*umi-one)))
! Eq. (47)
        tu=su*ur*((umi-one/uplus)*(one/u0-one/uf)+(umi**2-one/uplus**2)*log(uf/u0)+ &
           (a**2/uplus+one/uplus**3)*uplus*log((uf/uplus-one)/(u0/uplus-one))- &
           (a**2*umi+umi**3)*umi*log((uf*umi-one)/(u0*umi-one)))
! Eq. (49)
        lambdau=su*(one/u0-one/uf)
      elseif(ncase.eq.5) then
! This is the quartic case with one pair of complex roots
        p(4)=-1
! Table 1 Row 5
        f=-qs*one/abs(ee)/u1/u4
        g=(u4+u1)/u1/u4*f
        h=one
        if(u0.lt.uf) then
          p(5)=-4
! Fourth integral in Eq. (47)
          tu1=ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,0.d0,one,f,g,h,rffu0,u0,uf)
          p(5)=-2
! First three integrals in Eq. (47)
          tu2=ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,0.d0,one,f,g,h,rffu0,u0,uf)
          tu3=ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,one,-one/uplus,f,g,h,rffu0,u0,uf)
          tu4=ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,one,-umi,f,g,h,rffu0,u0,uf)
! Eq. (47)
          tu=su*ur/sqrt(-qs*ee)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Eq. (49)
          lambdau=su*tu1/sqrt(abs(ee))
! Minus sign in phi components is from flipping the sign of the u/u_\pm-1 factor to keep arguments positive.
          phiu0=-tu3
          phiu1=-tu4
! Eq. (48)
          phiu=su*ur/sqrt(-qs*ee)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
        elseif(u0.gt.uf) then
          p(5)=-4
! Fourth integral in Eq. (47)
          tu1=-ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,0.d0,one,f,g,h,rffu0,uf,u0)
          p(5)=-2
! First three integrals in Eq. (47)
          tu2=-ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,0.d0,one,f,g,h,rffu0,uf,u0)
          tu3=-ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,one,-one/uplus,f,g,h,rffu0,uf,u0)
          tu4=-ellquarticcomplex(p,-u1,one,u4*qs,-one*qs,one,-umi,f,g,h,rffu0,uf,u0)
! Eq. (47)
          tu=su*ur/sqrt(-qs*ee)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Minus sign in phi components is from flipping the sign of the u/u_\pm-1 factor to keep arguments positive.
          phiu0=-tu3
          phiu1=-tu4
! Eq. (48)
          phiu=su*ur/sqrt(-qs*ee)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
! Eq. (49)
          lambdau=su*tu1/sqrt(abs(ee))
        else
          tu=0.d0
          phiu=0.d0
          lambdau=0.d0
        endif        
      elseif(ncase.eq.6) then
! This is the quartic complex case with no real roots.
        p(4)=-1
! Table 1 Row 6
        h2=one/h1
        g1=dd/ee/(h2-h1)
        g2=-g1
        f1=one/sqrt(ee)
        f2=f1
        if(u0.lt.uf) then
          p(5)=-4
! Fourth integral in Eq. (47)
          tu1=elldoublecomplex(p,f1,g1,h1,f2,g2,h2,0.d0,one,rffu0,u0,uf)
          p(5)=-2
! First three integrals in Eq. (47)
          tu2=elldoublecomplex(p,f1,g1,h1,f2,g2,h2,0.d0,one,rffu0,u0,uf)
          tu3=elldoublecomplex(p,f1,g1,h1,f2,g2,h2,one,-one/uplus,rffu0,u0,uf)
          tu4=elldoublecomplex(p,f1,g1,h1,f2,g2,h2,one,-umi,rffu0,u0,uf)
! Minus sign in phi components is from flipping the sign of the u/u_\pm-1 factor to keep arguments positive.
          phiu0=-tu3
          phiu1=-tu4
! Eq. (47)
          tu=su*ur/sqrt(-qs*ee)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Eq. (48)
          phiu=su*ur/sqrt(-qs*ee)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
! Eq. (49)
          lambdau=su*tu1/sqrt(-qs*ee)
        elseif(u0.gt.uf) then
          p(5)=-4
! Fourth integral in Eq. (47)
          tu1=-elldoublecomplex(p,f1,g1,h1,f2,g2,h2,0.d0,one,rffu0,uf,u0)
          p(5)=-2
! First three integrals in Eq. (47)
          tu2=-elldoublecomplex(p,f1,g1,h1,f2,g2,h2,0.d0,one,rffu0,uf,u0)
          tu3=-elldoublecomplex(p,f1,g1,h1,f2,g2,h2,one,-one/uplus,rffu0,uf,u0)
          tu4=-elldoublecomplex(p,f1,g1,h1,f2,g2,h2,one,-umi,rffu0,uf,u0)
! Minus sign in phi components is from flipping the sign of the u/u_\pm-1 factor to keep arguments positive.
          phiu0=-tu3
          phiu1=-tu4
! Eq. (47)
          tu=su*ur/sqrt(-qs*ee)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Eq. (48)
          phiu=su*ur/sqrt(-qs*ee)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
! Eq. (49)
          lambdau=su*tu1/sqrt(-qs*ee)
        else
          tu=0.d0
          phiu=0.d0
        endif
      elseif(ncase.gt.6) then
! These are the quartic cases with all real roots
        p(4)=-1
        if(abs(u3-u2).lt.1.D-12) then
! These are the equal roots quartic cases
          if(u0.lt.uf) then
            if(u0.lt.u2) then
! Table 1 Row 7
              p(5)=-2
! First three integrals in Eq. (47)
              phiu1=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-one/uplus,rffu0,u0,uf)
              phiu2=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-umi,rffu0,u0,uf)
              tu2=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu0,u0,uf)
              tu3=phiu1
              tu4=phiu2
              p(5)=-4
! Fourth integral in Eq. (47)
              tu1=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu0,u0,uf)     
            elseif(u0.gt.u3) then
! Table 1 Row 8
! First three integrals in Eq. (47)
              phiu1=ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-one/uplus,rffu0,u0,uf)
              phiu2=ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-umi,rffu0,u0,uf)
              tu2=ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu0,u0,uf)
              tu3=phiu1
              tu4=phiu2
              p(5)=-4
! Fourth integral in Eq. (47)
              tu1=ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu0,u0,uf)
            else
              tu11=0.d0
              tu12=0.d0
              tu13=0.d0
              tu14=0.d0
              phiu11=0.d0
              phiu12=0.d0
            endif
! Eq. (47)
            tu=su*ur/sqrt(-qs*ee)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Eq. (48)
            phiu=su*ur/sqrt(-qs*ee)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
! Eq. (49)
            lambdau=tu1/sqrt(-qs*ee)
          elseif(u0.gt.uf) then
            if(u0.lt.u2) then
! Table 1 Row 7
              p(5)=-2
! First three integrals in Eq. (47)
              phiu1=-ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-one/uplus,rffu0,uf,u0)
              phiu2=-ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-umi,rffu0,uf,u0)
              tu2=-ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu0,uf,u0)
              tu3=-phiu1
              tu4=-phiu2
              p(5)=-4
! Fourth integral in Eq. (47)
              tu1=-ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu0,uf,u0)
            elseif(u0.gt.u3) then
! Table 1 Row 8
              p(5)=-2
! First three integrals in Eq. (47)
              phiu1=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-one/uplus,rffu0,uf,u0)
              phiu2=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-umi,rffu0,uf,u0)
              tu2=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu0,uf,u0)
              tu3=-phiu1
              tu4=-phiu2
              p(5)=-4
! Fourth integral in Eq. (47)
              tu1=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu0,uf,u0)
            else
              tu11=0.d0
              tu12=0.d0
              tu13=0.d0
              tu14=0.d0
              phiu11=0.d0
              phiu12=0.d0
            endif
! Eq. (47)
            tu=su*ur/sqrt(-qs*ee)*((umi-one/uplus)*tu1+(umi**2-one/uplus**2)*tu2- &
             (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu3+ &
             (2.d0*a*(a-l)+a**2*umi+umi**3)*tu4)
! Eq. (48)
            phiu=su*ur/sqrt(-qs*ee)*((l/uplus+2.d0*(a-l))*phiu0-(l*umi+2.d0*(a-l))*phiu1)
! Eq. (49)
            lambdau=tu1/sqrt(-qs*ee)    
          else
            tu=0.d0
            phiu=0.d0
            lambdau=0.d0
          endif
        elseif(u0.le.u2) then
! This is the quartic case with distinct, real roots
! Table 1 Row 7
          if(firstpt.and.(u0.ne.u2)) then
! Only compute integrals involving u0 once per geodesic
            p(5)=-2
! First three u0 integrals in Eq. (47)
            phiu01=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-one/uplus,rffu0,u0,u2)
            phiu02=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-umi,rffu0,u0,u2)
            tu02=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu0,u0,u2)
            tu03=phiu01
            tu04=phiu02
            p(5)=-4
! Fourth u0 integral in Eq. (48)
            tu01=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu0,u0,u2)
          elseif(u0.eq.u2) then
            tu01=0.d0
            tu02=0.d0
            tu03=0.d0
            tu04=0.d0
            phiu01=0.d0
            phiu02=0.d0
          else
            phiu01=tu03
            phiu02=tu04
          endif
          if(uf.ne.u2) then
            p(5)=-2
! First three uf integrals in Eq. (47)
            phiu11=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-one/uplus,rffu1,uf,u2)
            phiu12=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,one,-umi,rffu1,uf,u2)
            tu12=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu1,uf,u2)
            tu13=phiu11
            tu14=phiu12
            p(5)=-4
! Fourth uf integral in Eq. (47)
            tu11=ellquarticreal(p,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,one,rffu1,uf,u2)
          else
            tu11=0.d0
            tu12=0.d0
            tu13=0.d0
            tu14=0.d0
            phiu11=0.d0
            phiu12=0.d0
          endif
! Eq. (47) for u0 piece
          tu0=(umi-one/uplus)*tu01+(umi**2-one/uplus**2)*tu02- &
              (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu03+ &
              (2.d0*a*(a-l)+a**2*umi+umi**3)*tu04
! Eq. (47) for uf piece
          tu1=(umi-one/uplus)*tu11+(umi**2-one/uplus**2)*tu12- &
              (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu13+ &
              (2.d0*a*(a-l)+a**2*umi+umi**3)*tu14
          tu=su*(tu0-(-one)**tpr*tu1)/sqrt(abs(ee))*ur
! Eq. (48)
          phiu0=-(l/uplus+2.d0*(a-l))*phiu01+(l*umi+2.d0*(a-l))*phiu02
          phiu1=-(l/uplus+2.d0*(a-l))*phiu11+(l*umi+2.d0*(a-l))*phiu12
! Combine according to Eq. (54)
          phiu=su*(phiu0-(-one)**tpr*phiu1)/sqrt(abs(ee))*ur
! Eq. (49)
          lambdau=su*(tu01-(-one)**tpr*tu11)/sqrt(abs(ee))
        elseif(u0.ge.u3) then
          if(firstpt.and.(u0.ne.u3)) then
            p(5)=-2
! First three u0 integrals in Eq. (47)
            phiu01=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-one/uplus,rffu0,u3,u0)
            phiu02=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-umi,rffu0,u3,u0)
            tu02=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu0,u3,u0)
            tu03=phiu01
            tu04=phiu02
            p(5)=-4
! Fourth u0 integral in Eq. (47)
            tu01=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu0,u3,u0)
          elseif(u0.eq.u3) then
            tu01=0.d0
            tu02=0.d0
            tu03=0.d0
            tu04=0.d0
            phiu01=0.d0
            phiu02=0.d0
          else
            phiu01=tu03
            phiu02=tu04
          endif
          if(uf.ne.u3) then
            p(5)=-2
! First three uf integrals in Eq. (47)
            phiu11=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-one/uplus,rffu1,u3,uf)
            phiu12=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,one,-umi,rffu1,u3,uf)
            tu12=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu1,u3,uf)
            tu13=phiu11
            tu14=phiu12
            p(5)=-4
! Fourth uf integral in Eq. (47)
            tu11=-ellquarticreal(p,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,one,rffu1,u3,uf)
          else
            tu11=0.d0
            tu12=0.d0
            tu13=0.d0
            tu14=0.d0
            phiu11=0.d0
            phiu12=0.d0
          endif
! Eq. (47) for u0 piece
          tu0=(umi-one/uplus)*tu01+(umi**2-one/uplus**2)*tu02- &
              (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu03+ &
              (2.d0*a*(a-l)+a**2*umi+umi**3)*tu04
! Eq. (47) for uf piece
          tu1=(umi-one/uplus)*tu11+(umi**2-one/uplus**2)*tu12- &
              (2.d0*a*(a-l)+a**2/uplus+one/uplus**3)*tu13+ &
              (2.d0*a*(a-l)+a**2*umi+umi**3)*tu14
! Eq. (47)
          tu=su*(tu0-(-one)**tpr*tu1)/sqrt(abs(ee))*ur
! Eq. (48) for u0 piece
          phiu0=-(l/uplus+2.d0*(a-l))*phiu01+(l*umi+2.d0*(a-l))*phiu02
! Eq. (48) for uf piece
          phiu1=-(l/uplus+2.d0*(a-l))*phiu11+(l*umi+2.d0*(a-l))*phiu12
! Eq. (48)
          phiu=su*(phiu0-(-one)**tpr*phiu1)/sqrt(abs(ee))*ur
! Eq. (49) for u term
          lambdau=su*(tu01-(-one)**tpr*tu11)/sqrt(abs(ee))
        endif      
      endif
      if(ncase.gt.4) then
! Find roots of biquadratic M(mu).
        yy=-0.5d0*(a*a-ql2+sign(one,a*a-ql2)*sqrt((a*a-ql2)**2+4.d0*q2*a*a))
        if((a*a-ql2).lt.0.d0) then
          mneg=-yy/a/a
          mpos=q2/yy
        else
          mneg=q2/yy
          mpos=-yy/a/a
        endif
! Protect against rounding error in mpos:
        if(mpos.gt.1.d0) mpos=1.d0
        muplus=sqrt(mpos)
! NOTE: This formula uses a slightly different prescription for splitting up the mu integral.
!       All integrals are with respect to muplus, but the procedure of splitting into coefficients
!       and finding them by writing out specific cases is the same.
        a1=sm
        a2=sm*(-1.d0)**(tpm+1)
        a3=2.d0*int((2*tpm-sm+1)/4.d0)
        if(mneg.lt.0.d0) then
! Protect against rounding errors in roots:
          if(muplus.lt.mu0) then
            muplus=mu0
            mpos=muplus*muplus
          endif
! This is the symmetric roots case, where the orbit can cross the equatorial plane.
! Calculate phi, t mu component integrals:
          call calcphitmusym(a,mneg,mpos,mu0,muf,muplus,phimu1,phimu2,phimu3,tmu1, &
                             tmu2,tmu3,rffmu1,rffmu2,rffmu3,rdc,rjc,firstpt)
! Eq. (45)
          tmu=a*a*mneg*iu+(a1*tmu1+a2*tmu2+a3*tmu3)
! Eq. (46)
          phimu=-l*iu+l*(a1*phimu1+a2*phimu2+a3*phimu3)
       else
! This is the asymmetric roots case
          if(sign(1.d0,mu0).eq.-1) muplus=-muplus
! Protect for rounding error when mu0 is a turning point:
          if(abs(muplus).lt.abs(mu0)) then
            muplus=mu0
            mpos=muplus*muplus
          endif
          if(abs(mneg).gt.mu0*mu0) then
            mneg=mu0*mu0
          endif
! Calculate phi, t mu component integrals:
          call calcphitmuasym(a,mneg,mpos,mu0,muf,muplus,phimu1,phimu2,phimu3,tmu1, &
                              tmu2,tmu3,rffmu1,rffmu2,rffmu3,rdc,rjc,firstpt)
! Eq. (45)
          tmu=(a1*tmu1+a2*tmu2+a3*tmu3)
! Eq. (46)    
          phimu=-l*iu+l*(a1*phimu1+a2*phimu2+a3*phimu3)
        endif                
      endif
! Eq. (49)
      lambda=lambdau+tmu
      if(l.eq.0.d0) phiu=phiu-sign(1.d0,a)*pi*tpm
      return
      end
!*********************************************************************************************
      subroutine calcphitmuasym(a,mneg,mpos,mu0,muf,muplus,phimu0,phimuf, &
      phimum,tmu0,tmuf,tmum,rf0,rff,rfc,rdc,rjc,firstpt)
!*********************************************************************************************
!     PURPOSE: Computes integral pieces of phimu and tmu in the asymmetric (M_- > 0) case.
!
!     INPUTS:  A -- Black hole spin parameter.
!              MNEG, MPOS -- Roots of quadratic in \mu^2 M(\mu) in increasing order.
!              MU0 -- Initial \mu.
!              MUF -- Final \mu.
!              MUPLUS -- \sqrt{mpos}. Upper physical turning point.
!              RF0 -- RF term for Legendre elliptic integral of the first kind from MU0 to MUPLUS.
!              RFF -- RF term for Legendre elliptic integral of the first kind from MUF to MUPLUS.
!              RFC -- RF term for complete Legendre elliptic integral of the first kind.              
!              FIRSTPT -- Boolean variable. If FIRSTPT=.TRUE., compute RDC and RJC. Otherwise they're
!                         given as inputs.
!     OUTPUTS: PHIMU0 -- Computes phimu integral from MU0 to MUPLUS.
!              PHIMUF -- Computes phimu integral from MUF to MUPLUS.
!              PHIMUM -- Computes phimu integral from MUMINUS to MUPLUS.
!              TMU0 -- Computes tmu integral from MU0 to MUPLUS.
!              TMUF -- Computes tmu integral from MUF to MUPLUS. 
!              TMUM -- Computes tmu integral from -MUPLUS to MUPLUS.
!              RDC -- RD term in complete Legendre's elliptic integral of the second kind. Computed if
!                        FIRSTPT=.TRUE., and input otherwise.
!              RJC -- RJ term in complete Legendre's elliptic integral of the third kind. Computed if
!                        FIRSTPT=.TRUE., and input otherwise.
!     ROUTINES CALLED: ELLPHITMU             
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
!     Calculate phimu in terms of Legendre elliptic integrals for
!     case with asymmetric roots, M_- > 0.
      implicit none
      double precision a,mneg,mpos,mu0,muf,muplus,phimu0,phimuf,phimum,tmu0,tmuf,tmum,fac2
      double precision fac,m1,n,elle0,ellef,ellec,ellpi0,ellpif,ellpic,phi0,phif,rf0,rff,rfc,rdc,rjc
      logical firstpt
! After Eq. (45):
      fac=abs(a)*muplus
      m1=mneg/mpos
! If the orbit crosses the pole, don't compute phi integral terms.
      if(mpos.ne.1.d0) then
! After Eq. (46)
        n=(mpos-mneg)/(1.d0-mpos)
! Additional piece of fac, separate so that the phi terms don't blow up when the orbit crosses the pole.
        fac2=1.d0-mpos
      else
! Set n to a value we can detect:
        n=-1.d0
        fac2=1.d0
      endif
      phi0=asin(sqrt((mpos-mu0*mu0)/(mpos-mneg)))
      phif=asin(sqrt((mpos-muf*muf)/(mpos-mneg)))
! Get Legendre integrals of 1st/3rd kind:
      call ellphitmu(phi0,phif,m1,-n,firstpt,rf0,rff,rfc,rdc,rjc,elle0,ellef,ellec,ellpi0,ellpif,ellpic)
! Now calculate required integrals
      if(firstpt) then
! Eq. (45) for the mu0 term and the integral between the turning points. 
! The factors of two in tmum and phimum set the integral between turning points equal to the complete integral.
        tmu0=fac*elle0
        tmum=fac*ellec/2.d0
! Eq. (46) for the mu0 term and the integral between turning points.
        phimu0=ellpi0/fac2/fac
        phimum=ellpic/fac2/fac/2.d0
      endif
! Eq. (45) for the muf term
      tmuf=fac*ellef
! Eq. (46) for the muf term.
      phimuf=ellpif/fac2/fac
      return
      end
!*************************************************************************************
      subroutine calcphitmusym(a,mneg,mpos,mu0,muf,muplus,phimu0,phimuf,phimum, &
                  tmu0,tmuf,tmum,rf0,rff,rfc,rdc,rjc,firstpt)
!******************************************************************************************************
!     PURPOSE: Computes integral pieces of phimu and tmu in the symmetric (M_- < 0) case.
!
!     INPUTS:  A -- Black hole spin parameter.
!              MNEG, MPOS -- Roots of quadratic in \mu^2 M(\mu) in increasing order.
!              MU0 -- Initial \mu.
!              MUF -- Final \mu.
!              MUPLUS -- \sqrt{mpos}. Upper physical turning point.
!              RF0 -- RF term for Legendre elliptic integral of the first kind from MU0 to MUPLUS.
!              RFF -- RF term for Legendre elliptic integral of the first kind from MUF to MUPLUS.
!              RFC -- RF term for complete Legendre elliptic integral of the first kind.              
!              FIRSTPT -- Boolean variable. If FIRSTPT=.TRUE., compute RDC and RJC. Otherwise they're
!                         given as inputs.
!     OUTPUTS: PHIMU0 -- Computes phimu integral from MU0 to MUPLUS.
!              PHIMUF -- Computes phimu integral from MUF to MUPLUS.
!              PHIMUM -- Computes phimu integral from MUMINUS to MUPLUS.
!              TMU0 -- Computes tmu integral from MU0 to MUPLUS.
!              TMUF -- Computes tmu integral from MUF to MUPLUS. 
!              TMUM -- Computes tmu integral from -MUPLUS to MUPLUS.
!              RDC -- RD term in complete Legendre's elliptic integral of the second kind. Computed if
!                        FIRSTPT=.TRUE., and input otherwise.
!              RJC -- RJ term in complete Legendre's elliptic integral of the third kind. Computed if
!                        FIRSTPT=.TRUE., and input otherwise.
!     ROUTINES CALLED: ELLPHITMU                
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
      double precision a,mneg,mpos,mu0,muf,muplus,phimu0,phimuf,phimum,tmu0,tmuf,tmum,fac2
      double precision fac,m1,n,elle0,ellef,ellec,ellpi0,ellpif,ellpic,phi0,phif,rf0,rff,rfc,rdc,rjc
      logical firstpt
! Immediately after Eq. (45):
      fac=abs(a)*sqrt(mpos-mneg)
      m1=-mneg/(mpos-mneg)
! If the orbit crosses the pole, don't compute phi integral terms.
      if(mpos.ne.1.d0) then
! Immediately after Eq. (46)
        n=mpos/(1.d0-mpos)
! Additional piece of fac, separate so that the phi terms don't blow up when the orbit crosses the pole.
        fac2=1.d0-mpos
      else
! Set n to a value we can detect:
        n=-1.d0
        fac2=1.d0
      endif
! Calculate phi arguments for Legendre integrals (in the paper, x=sin(phi)):
      phi0=acos(mu0/muplus)
      phif=acos(muf/muplus)
! Next use ellpi to calculate other pieces if non-zero:
      call ellphitmu(phi0,phif,m1,-n,firstpt,rf0,rff,rfc,rdc,rjc,elle0,ellef,ellec,ellpi0,ellpif,ellpic)
! Calculate integrals involving mu0 and turning points only if this is the first point on the geodesic
      if(firstpt) then
! Eq. (45) for the mu0 term and the integral between turning points.
        tmu0=fac*elle0
        tmum=fac*ellec
! Eq. (46) for the mu0 term and the integral between turning points.
        phimu0=ellpi0/fac2/fac
        phimum=ellpic/fac2/fac
      endif
! Eq. (45) for the muf term.
      tmuf=fac*ellef
! Eq. (46) for the muf term.
      phimuf=ellpif/fac2/fac
      return
      end
!******************************************************************************************************
      subroutine ellphitmu(phi0,phif,m1,n,firstpt,rf0,rff,rfc,rdc,rjc,elle0,ellef,ellec,ellpi0,ellpif,ellpic)
!******************************************************************************************************
!     PURPOSE: Computes Legendre elliptic integrals F(x|m), E(x|m) and Pi(n;x|m) for MU0, MUF and MUPLUS pieces.
!
!     INPUTS:  Arguments above with x = sin(phi) and m=1-k^2. n is input with the Numerical Recipes convention.
!              RF0 -- RF term for Legendre elliptic integral of the first kind from MU0 to MUPLUS.
!              RFF -- RF term for Legendre elliptic integral of the first kind from MUF to MUPLUS.
!              RFC -- RF term for complete Legendre elliptic integral of the first kind.
!              FIRSTPT -- Boolean variable. If FIRSTPT=.TRUE., compute RDC and RJC. Otherwise they're
!                         given as inputs.
!     OUTPUTS: ELLE0 -- Legendre's elliptic integral of the second kind from MU0 to MUPLUS.
!              ELLEF -- Legendre's elliptic integral of the second kind from MUF to MUPLUS.
!              ELLEC -- Complete Legendre's elliptic integral of the second kind.
!              ELLPI0 -- Legendre's elliptic integral of the third kind from MU0 to MUPLUS.
!              ELLEPIF -- Legendre's elliptic integral of the third kind from MUF to MUPLUS. 
!              ELLEPIC -- Complete Legendre elliptic integral of the third kind.
!              RDC -- RD term in complete Legendre's elliptic integral of the second kind. Computed if
!                        FIRSTPT=.TRUE., and input otherwise.
!              RJC -- RJ term in complete Legendre's elliptic integral of the third kind. Computed if
!                        FIRSTPT=.TRUE., and input otherwise.
!     ROUTINES CALLED: ELLCUBICREAL, ELLCUBICCOMPLEX, ELLQUARTICREAL, ELLQUARTICCOMPLEX, ELLDOUBLECOMPLEX,
!                       TFNKERR, PHIFNKERR, CALCPHITMUSYM, CALCPHITMUASYM                   
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
      double precision rj,rd
      double precision phi0,phif,m1,n,ellpif,ellpi0,ellpic,elle0,ellef,ellec
      double precision pi,s0,sf,ak,q0,qf,rf0,rff,rfc,rj0,rjf,rjc,rd0,rdf,rdc
      logical firstpt
      pi=acos(-1.d0)
! Convert to argument k from complimentary parameter m1
      ak=sqrt(1.d0-m1)
! First do mu0, muplus components if they haven't already been calculated:
      if(firstpt) then
        s0=sin(phi0)
        q0=(1.d0-s0*ak)*(1.d0+s0*ak)
! Compute Carlson integrals for Legendre's of the 2nd kind using Press et al (1992) 6.11.20:
        rd0=rd(1.d0-s0*s0,q0,1.d0)
        rdc=rd(0.d0,m1,1.d0)
        elle0=s0*(rf0-((s0*ak)**2)*rd0/3.d0)
        ellec=2.d0*(rfc-ak*ak*rdc/3.d0)
! n only equals 1 if it was set that way, in which case the phi terms are zero.
        if(n.ne.1.d0) then
! Now handle Legendre's 3rd from Press et al (1992) 6.11.21:
          rj0=rj(1.d0-s0*s0,q0,1.d0,1.d0-n*s0*s0)
          rjc=rj(0.d0,m1,1.d0,1.d0-n)
          ellpi0=s0*(rf0+n*s0*s0*rj0/3.d0)
          ellpic=2.d0*(rfc+n*rjc/3.d0)
        else
          ellpi0=0.d0
          ellpic=0.d0
        endif
! The Press et al (1992) routines are only valid for 0 le phi le pi/2. If phi gt pi/2,
! we've computed int_0^pi/2-int_pi/2^phi. To get correct value, do int_0^phi = 2 int_0^pi/2-(int_0^pi/2-int_pi/2^phi):
        if (phi0.gt.pi/2.d0) then
          ellpi0=ellpic-ellpi0
          elle0=ellec-elle0
        endif
      endif
! Repeat procedure for muf part:
      sf=sin(phif)
      qf=(1.d0-sf*ak)*(1.d0+sf*ak)
      rdf=rd(1.d0-sf*sf,qf,1.d0)
      ellef=sf*(rff-((sf*ak)**2)*rdf/3.d0)
      if(n.ne.1.d0) then
        rjf=rj(1.d0-sf*sf,qf,1.d0,1.d0-n*sf*sf)
        ellpif=sf*(rff+n*sf*sf*rjf/3.d0)
      else
        ellpif=0.d0
      endif
! See above note on phi > pi/2.
      if (phif.gt.pi/2.d0) then
        ellpic=2.d0*(rfc+n*rjc/3.d0)
        ellec=2.d0*(rfc-ak*ak*rdc/3.d0)
        ellpif=ellpic-ellpif
        ellef=ellec-ellef
      endif
      return
      end
!******************************************************************************************************
      double precision function phifnkerr(u,u1,u2,l,a)
!******************************************************************************************************
!     PURPOSE: Computes phiu for equal roots cubic cases.
!
!     INPUTS:  u -- Value of u at which to compute phiu.
!              u1,u2 -- Roots in increasing order, with u2=u3.
!              l -- Dimensionless angular momentum.
!              a -- Black hole spin parameter.
!     OUTPUTS: phiu
!     ROUTINES CALLED: *                 
!     ACCURACY:   Machine.
!     REMARKS: *
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
! Computes phiu for equal roots cubic cases
! JAD 2/11/2008
      double precision u,u1,u2,l,a,up,umi,s,su21,su1,sup,sum,fn
      up=1.d0/(1.d0+sqrt(1.d0-a**2))
      umi=1.d0-sqrt(1.d0-a**2)
      s=sqrt(u-u1)
      su21=sqrt(u2-u1)
      su1=sqrt(-u1)
      sup=sqrt(1-u1/up)
      sum=sqrt(1-u1*umi)
      fn=(l+2.d0*a*u2-2.d0*l*u2)/(su21*(u2*umi-1.d0)*(u2/up-1.d0))*log((su21+s)/(su21-s))+ &
          (2.d0*a+l*(-2.d0+umi))*sqrt(umi)*log((sum+s*sqrt(umi))/(sum-s*sqrt(umi)))/(sum*(u2*umi-1.d0)*(umi-1.d0/up)) &
          +(2.d0*a+l*(-2.d0+1.d0/up))*sqrt(1.d0/up)*log((sup+sqrt(1.d0/up)*s)/(sup-sqrt(1.d0/up)*s)) &
          /(1.d0/up-umi)*sup*(u2/up-1.d0)
      phifnkerr=fn
      return
      end
            !******************************************************************************************************
      double precision function tfnkerr(u0,uf,u1,u2,l,a)
!******************************************************************************************************
!     PURPOSE: Computes tu for equal roots cubic cases.
!
!     INPUTS:  u0 -- Initial u variable.
!              uf -- Final u.
!              u1,u2 -- Roots in increasing order, with u2=u3.
!              l -- Dimensionless angular momentum.
!              a -- Black hole spin parameter.
!     OUTPUTS: phiu
!     ROUTINES CALLED: *                 
!     ACCURACY:   Machine.
!     REMARKS: *
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
! Computes tu for equal roots cubic cases
! JAD 2/10/2008
      double precision up,umi,sf,ss,su21,su1,sup,sum,fn,u0,uf,u1,u2,l,a
      up=1.d0/(1.d0+sqrt(1.d0-a**2))
      umi=1.d0-sqrt(1.d0-a**2)
      sf=sqrt(uf-u1)
      ss=sqrt(u0-u1)
      su21=sqrt(u2-u1)
      su1=sqrt(-u1)
      sup=sqrt(1-u1/up)
      sum=sqrt(1-u1*umi)
      fn=sf/(uf*u1*u2)-ss/(u0*u1*u2)-((-u2-2.d0*u1*(1.d0+u2*umi+u2/up))*log((sf+su1)*(su1-ss)/((-sf+su1)*(ss+su1)))) &
          /(2.d0*(-u1)**(3.d0/2.d0)*u2**2) &
          + ((1.d0-2.d0*a*l*u2**3+a**2*u2**2*(1.d0+2.d0*u2))*log((su21+sf)*(su21-ss)/((su21-sf)*(su21+ss)))) &
          /(u2**2*su21*(u2*umi-1.d0)*(u2/up-1.d0)) &
          + ((-2.d0*a*l+a**2*(2.d0+umi)+umi**3)*sqrt(umi)*log((sum+sf*sqrt(umi)) &
           *(sum-ss*sqrt(umi))/((sum-sf*sqrt(umi)) &
          *(sum+ss*sqrt(umi))))) &
          /(sum*(u2*umi-1.d0)*(umi-1/up)) &
          +((-2.d0*a*l+a**2*(2.d0+1/up)+1.d0/up**3)*sqrt(1.d0/up)*log((sup+sf*sqrt(1.d0/up))*(sup-ss*sqrt(1/up)) &
          /((sup-sf*sqrt(1.d0/up))*(sup+ss*sqrt(1/up)))) &
          /((1.d0/up-umi)*sup*(u2/up-1.d0)))
      tfnkerr=fn
      return
      end
!*****************************************************************************
      subroutine geor(u0,uf,mu0,muf,a,l,l2,q2,imu,tpm,tpr,su,sm,ncase,h1, &
           u1,u2,u3,u4,rffu0,rffu1,rffmu1,rffmu2,rffmu3,iu0,i1mu,i3mu,pht,firstpt)
!******************************************************************************
!     PURPOSE: Computes final radial coordinate corresponding to the final polar angle
!              MUF given initial coordinates U0, MU0 and constants of the motion.
!
!     INPUTS:   U0 -- Starting u value. If U0=0, DTI and LAMBDAI values will blow up.
!               MU0 -- Starting mu=cos(theta) value.
!               MUF -- Final mu value.
!               A -- Black hole spin, on interval [0,1).
!               L -- Dimensionless z-component of angular momentum.
!               L2 -- L*L
!               Q2 -- Dimensionless Carter's constant.
!               TPR -- Number of u turning points reached between U0 and UF.
!               SU -- Initial du/dlambda, =1 (-1) for ingoing (outgoing) rays.
!               SM -- Initital dmu/dlambda.
!               PHT -- Boolean variable. If .TRUE., RFFU1 is computed for use in
!                      SUBROUTINE GEOPHITIME.
!               FIRSTPT -- Boolean variable. If .TRUE., roots of U(u) U1,2,3,4 and NCASE are computed as well as H1
!                           if NCASE=6
!     OUTPUTS:  UF -- Final inverse radius.
!               IMU -- Value of IMU integral between MU0 and MUF.
!               TPM -- Number of mu turning points reached between MU0 and MUF.
!               NCASE -- Case number corresponding to Table 1. Output if FIRSTPT=.TRUE., input otherwise.
!               H1 -- Value of h1 from Eq. (21) for given constants of motion if NCASE=6. Output if FIRSTPT=.TRUE.,
!                     input otherwise.
!               U1,U2,U3,U4 -- Increasing (real) roots. Output if FIRSTPT=.TRUE., input otherwise.
!               RFFU0 -- Value of RF relevant for U0 piece of IU integral. Computed if FIRSTPT=.TRUE., input otherwise.
!               RFFU1 -- Value of RF relevant for UF piece of IU integral. Computed if PHT=.TRUE.
!               RFFMU1 -- Value of RF relevant for MU0 piece of IMU integral. Computed if FIRSTPT=.TRUE., input
!                         otherwise.
!               RFFMU2 -- Value of RF relevant for MUF piece of IMU integral. Computed if PHT=.TRUE.
!               RFFMU3 -- Value of RF relevant for turning point piece of IMU integral. Computed if FIRSTPT=.TRUE.,
!                         input otherwise.
!               IU0 -- Value of IU integral between U0 and relevant turning point if one exists. Computed if
!                      u turning point present and FIRSTPT=.TRUE., input otherwise. Ignored if no turning point
!                      is present.
!               I1MU -- Value of IMU integral between MU0 and MUPLUS. Computed if FIRSTPT=.TRUE., input otherwise.
!               I3MU -- Value of IMU integral between MUMINUS and MUPLUS. Computed if FIRSTPT=.TRUE., input otherwise.
!     ROUTINES CALLED: SNCNDN, ZROOTS, ASECH, CALCIMUSYM, CALCIMUSYMF, CALCIMUASYM, CALCIMUASYMF, ELLCUBICREAL, 
!                       ELLCUBICCOMPLEX, ELLQUARTICREAL, ELLQUARTICCOMPLEX, ELLDOUBLECOMPLEX                   
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      implicit none
! Given a starting position (u0,mu0) where u0=1/r0, mu0=cos(theta0),
! and the final polar angle, muf, this subroutine calculates the final
! radius, uf, for a geodesic with parameters (l,q2)
! JAD 2/20/2009
      integer i,nreal,parr(5)
      double precision a,aa,a1,a2,a3,c1,c2,c3,c4,c5,cn,dn,dis,rr,i1mu,i3mu,ufold,dummy, &
            f,half,iu,qs,h1,h2,f1,f2,g1,g2,l,l2,m1,mneg,mpos,mu0,muf,muplus,uarg, &
            one,pi,pi2,q2,ql2,rf,s1,sn,sm,su,theta,third,two,bb,farg,sarg,rffu0,rffu1, &
            u0,uf,u1,u2,u3,u4,g,cc,dd,ee,iu0,iu1,ellcubicreal,ellcubiccomplex,ellquarticreal, &
            ellquarticcomplex,elldoublecomplex,asech,muminus,iarg,qq,rb2,uplus,yy,realroot(4), &
            i2mu,imu,jarg,cd2,dc2,sc,sc2,three,m,n,n2,p,r,ua,ub,mn2,pr2,temp,karg,pm, &
            rffmu1,rffmu2,rffmu3,iut
      integer tpm,tpr,ncase
      logical pht,firstpt
      complex*16 c(5),root(4),coefs(7),hroots(6)
      parameter ( one=1.D0, two=2.D0, half=0.5D0, third = 0.3333333333333333D0, three=3.D0 )
      pi=acos(-one)
      pi2=two*pi
      uplus=one/(one+sqrt(one-a*a))
      ql2=q2+l2
!  Eq. (22): Coefficients of quartic in U(u)
      cc=a*a-q2-l2
      dd=two*((a-l)**2+q2)
      ee=-a*a*q2
      if(q2.eq.0.d0) then
! Calculate imu in the special case q2=0. In this case there can only be 0 or 1 mu turning points.
        if(l2.ge.a*a.or.mu0.eq.0.d0) then
          imu=0.d0
          uf=-1.d0
          ncase=0
          return
        else
          s1=sign(1.d0,mu0)
          a1=s1*sm
          a2=s1*sm*(-one)**(tpm+1)
          muplus=s1*sqrt(one-l2/a/a)
          i1mu=one/abs(a*muplus)*asech(mu0/muplus)
          i2mu=one/abs(a*muplus)*asech(muf/muplus)
          imu=a1*i1mu+a2*i2mu
        endif
      elseif(a.eq.0.d0) then
! Calculate imu in the special case a=0
        a1=sm
        muplus=sqrt(q2/ql2)
        if(mu0.gt.muplus) muplus=mu0
        i1mu=(half*pi-asin(mu0/muplus))/sqrt(ql2)
        i3mu=pi/sqrt(ql2)
        a2=sm*(-one)**tpm
        a3=two*int((two*dble(tpm)+3.d0-sm)/4.d0)-one
        i2mu=(half*pi+asin(muf/muplus))/sqrt(ql2)
        imu=a1*i1mu+a2*i2mu+a3*i3mu
      endif
!  Determine if U is cubic or quartic and find roots.
      if((ee.eq.0.d0) .and. (dd.ne.0.d0)) then
        parr(1)=-1
        parr(2)=-1
        parr(3)=-1
        parr(4)=0
        parr(5)=0
        qq=cc*cc/dd/dd/9.d0
        rr=(two*cc**3/dd**3+27.d0/dd)/54.d0
        dis=rr*rr-qq**3
        if(dis.lt.-1.d-16) then
! These are the cubic real roots cases with u1<0<u2<=u3
          theta=acos(rr/qq**1.5d0)
          u1=-two*sqrt(qq)*cos(theta/3.d0)-cc/dd/3.d0
          u2=-two*sqrt(qq)*cos((theta-two*pi)/3.d0)-cc/dd/3.d0
          u3=-two*sqrt(qq)*cos((theta+two*pi)/3.d0)-cc/dd/3.d0
80        continue
          if(u0.le.u2) then
            ncase=1
            if(firstpt.and.(u0.ne.u2)) then
              iu0=ellcubicreal(parr,-u1,one,u2,-one,u3,-one,0.d0,0.d0,rffu0,u0,u2)
            elseif(u0.eq.u2) then
              iu0=0.d0
            endif
! Table 2 Row 1
            m1=(u3-u2)/(u3-u1)
            jarg=sqrt((u3-u1)*dd)*half*(imu-su*iu0/sqrt(dd))
            call sncndn(jarg,m1,sn,cn,dn)
            cd2=cn**2/dn**2
            uf=u1+(u2-u1)*cd2
            tpr=(sign(1.d0,su*(imu-su*iu0/sqrt(dd)))+1.d0)/2.d0
            if(pht) iu1=ellcubicreal(parr,-u1,one,u2,-one,u3,-one,0.d0,0.d0,rffu1,uf,u2)/sqrt(dd)
          elseif(u0.ge.u3) then
            ncase=2
            if(firstpt.and.(u0.ne.u3)) then
              iu0=ellcubicreal(parr,-u1,one,-u2,one,-u3,one,0.d0,0.d0,rffu0,u3,u0)
            elseif(u0.ne.u3) then
              iu0=0.d0
            endif
! Table 2 Row 2
            m1=(u3-u2)/(u3-u1)
            jarg=sqrt((u3-u1)*dd)*half*(imu+su*iu0/sqrt(dd))
            call sncndn(jarg,m1,sn,cn,dn)
            dc2=dn**2/cn**2
            dummy=uf
            uf=u1+(u3-u1)*dc2
            tpr=(-sign(1.d0,su*(imu+iu0/sqrt(dd)))+1.d0)/2.d0
            if(pht) iu1=ellcubicreal(parr,-u1,one,-u2,one,-u3,one,0.d0,0.d0,rffu1,u3,uf)/sqrt(dd)
          else
            write(6,*) 'WARNING - Unphysical Cubic Real. Input modified.'
            if(su.eq.1) then
              u0=u3
            else
              u0=u2
            endif
            goto 80
          endif
        elseif(abs(dis).lt.1.d-16) then
          tpr=0
! This is a cubic case with equal roots.
          u1=-two*sqrt(qq)-cc/dd/3.d0
          u2=-two*sqrt(qq)*cos((two*pi)/3.d0)-cc/dd/3.d0
          u3=u2
          tpr=0
          if(u0.le.u2) then
            ncase=1
            if(uf.gt.u2) then 
              uf=u2
              iu=su*1.d300
            else
              sarg=sqrt((u0-u1)/(u2-u1))
              jarg=sqrt((u2-u1)*dd)*half*su*imu+half*log((one+sarg)/(one-sarg))
              uf=u1+(u2-u1)*tanh(jarg)**2
            endif           
          elseif(u0.ge.u2) then
            ncase=2
            if(uf.lt.u2) then
              uf=u2
              iu=su*1.d300
            else
              sarg=sqrt((u2-u1)/(u0-u1))
              jarg=-sqrt((u2-u1)*dd)*half*su*imu+half*log((one+sarg)/(one-sarg))
              uf=u1+(u2-u1)/tanh(jarg)**2    
            endif
          endif
        else
! This is a cubic complex case with one real root.
          ncase=3
! Table 2 Row 3
          tpr=0
          aa=-sign(1.d0,rr)*(abs(rr)+sqrt(dis))**third
          if(aa.ne.0.d0) then
            bb=qq/aa
          else
            bb=0.d0
          endif
          u1=(aa+bb)-cc/dd/3.d0
          f=-one/dd/u1
          g=f/u1
! Make sure there is a valid solution.
          if(su.gt.0.d0) then 
            iut=ellcubiccomplex(parr,-u1,one,0.d0,0.d0,f,g,one,dummy,u0,uplus)/sqrt(dd)
          else
            iut=ellcubiccomplex(parr,-u1,one,0.d0,0.d0,f,g,one,dummy,0.d0,u0)/sqrt(dd)
          endif
          if(imu.gt.iut) then
            uf=-1.d0
            ncase=0
            tpr=0
            return
          endif
          m=-g/2.d0
          if(firstpt) iu0=su*ellcubiccomplex(parr,-u1,one,0.d0,0.d0,f,g,one,dummy,u1,u0)/sqrt(dd)
          c3=-one
          if(a.ne.0.d0.or.l.ne.0.d0) c3=(a+l)/(a-l)
          c2=sqrt(u1*(three*u1+c3))
          c1=sqrt(c2*dd)
          m1=half+(6.d0*u1+c3)/(8.d0*c2)
          jarg=c1*(imu+iu0)
          call sncndn(jarg,m1,sn,cn,dn)
          uf=(c2+u1-(c2-u1)*cn)/(one+cn)
          if(pht) iu=ellcubiccomplex(parr,-u1,one,0.d0,0.d0,f,g,one,rffu0,u0,uf)/sqrt(dd)
        endif
      elseif(ee.eq.0.d0 .and. dd.eq.0.d0) then
! This is the special case where q2=0 and l=a and mu=0 at all times, so we can't invert.
        ncase=4
        uf=-1.D0
        tpr=0
      else
! Find roots of M(mu) in biquadratic case.
        yy=-0.5d0*(a*a-ql2+sign(one,a*a-ql2)*sqrt((a*a-ql2)**2+4.d0*q2*a*a))
        if((a*a-ql2).lt.0.d0) then
          mneg=-yy/a/a
          mpos=q2/yy
        else
          mneg=q2/yy
          mpos=-yy/a/a
        endif
        muplus=sqrt(mpos)
! NOTE: This formula uses a slightly different prescription for splitting up the mu integral.
!       All integrals are with respect to muplus.
        a1=sm
        a2=sm*(-1.d0)**(tpm+1)
        a3=2.d0*int((2*tpm-sm+1)/4.d0)
        if(mneg.lt.0.d0) then
! This is the symmetric roots case, where the orbit can cross the equatorial plane.
          if(mu0.gt.muplus) then
            muplus=mu0
            mpos=muplus*muplus
          endif
! Compute integrals involving u0 and turning points once per geodesic:
          if(firstpt) call calcimusym(a,mneg,mpos,mu0,muplus,i1mu,i3mu,rffmu1,rffmu3)
          call calcimusymf(a,mneg,mpos,muf,muplus,i2mu,i3mu,rffmu2)
          imu=a1*i1mu+a2*i2mu+a3*i3mu
        else
! This is the asymmetric roots case.
          if(abs(muf).lt.sqrt(mneg)) then
            uf=-1.d0
            ncase=0
            tpr=0
            return
          else
            if(sign(1.d0,mu0).eq.-1) muplus=-muplus
            if(abs(muplus).lt.abs(mu0)) then
              muplus=mu0
              mpos=muplus*muplus
            endif
            mneg=min(mu0*mu0,mneg)
            if(firstpt) call calcimuasym(a,mneg,mpos,mu0,muplus,i1mu,i3mu,rffmu1,rffmu3)
            call calcimuasymf(a,mneg,mpos,muf,muplus,i2mu,i3mu,rffmu2)
          endif
          imu=a1*i1mu+a2*i2mu+a3*i3mu
        endif
! These are the quartic cases. First we find the roots.
        parr(1)=-1
        parr(2)=-1
        parr(3)=-1
        parr(4)=-1
        parr(5)=0
        if(firstpt) then
          c(1)=dcmplx(one,0.d0)
          c(2)=dcmplx(0.d0,0.d0)
          c(3)=dcmplx(cc,0.d0)
          c(4)=dcmplx(dd,0.d0)
          c(5)=dcmplx(ee,0.d0)
          call zroots(c,4,root,.true.)
          nreal=0
          do i=1,4
            if(dimag(root(i)).eq.0.d0) nreal=nreal+1
          enddo
          if(nreal.eq.2) then
! This is the quartic complex case with 2 real roots.
            ncase=5
            tpr=0
            u1=dble(root(1))
            if(dimag(root(2)).eq.0.d0) then
              u4=dble(root(2))
            else
              u4=dble(root(4))
            endif
          elseif(nreal.eq.0) then
            ncase=6
            tpr=0
          else
            u1=dble(root(1))
            u2=dble(root(2))
            u3=dble(root(3))
            u4=dble(root(4))
90          continue
            if(u2.gt.uplus.and.u3.gt.uplus) then
              ncase=5
            elseif(u0.le.u2) then
              ncase=7
            elseif(u0.ge.u3) then
              ncase=8
            else
              write(6,*) 'WARNING--Unphysical Quartic Real. Inputs modified.'
              if(su.eq.1) then
                u0=u3
              else
                u0=u2
              endif
              goto 90
            endif             
          endif
        endif
        if(ncase.eq.5) then
! Table 2 Row 5
          qs=sign(1.d0,q2)
          f=-qs*one/abs(ee)/u1/u4
          g=(u4+u1)/u1/u4*f
! Make sure there is a valid solution.
          if(su.gt.0.d0) then
            iut=ellquarticcomplex(parr,-u1,one,qs*u4,-qs*one,0.d0,0.d0,f,g,one,dummy,u0,uplus)/sqrt(abs(ee))
          else
            iut=ellquarticcomplex(parr,-u1,one,qs*u4,-qs*one,0.d0,0.d0,f,g,one,dummy,0.d0,u0)/sqrt(abs(ee))
          endif
          if(imu.gt.iut) then
            uf=-1.d0
            ncase=0
            tpr=0
            return
          endif
          if(qs.eq.one) then
            ua=u4
            ub=u1
          else
            ua=u1
            ub=u4
          endif
          m=-g*half
          n2=f-g**2/4.d0
          c4=sqrt((m-u4)**2+n2)
          c5=sqrt((m-u1)**2+n2)
          c1=sqrt(abs(ee*c4*c5))
          if(firstpt) iu0=su*ellquarticcomplex(parr,-u1,one,qs*u4,-qs*one,0.d0,0.d0,f,g,one,dummy,ub,u0)/sqrt(abs(ee))
          m1=qs*((c4+qs*c5)**2-(u4-u1)**2)/(4.d0*c4*c5)
          jarg=c1*(imu+iu0)
          call sncndn(jarg,m1,sn,cn,dn)
          uf=(u4*c5+qs*u1*c4-(qs*u4*c5-u1*c4)*cn)/((c4-qs*c5)*cn+(qs*c4+c5))
          if(pht) iu=ellquarticcomplex(parr,-u1,one,qs*u4,-qs*one,0.d0,0.d0,f,g,one,rffu0,u0,uf)/sqrt(abs(ee))
        elseif(ncase.eq.6) then
! This is the quartic complex case with no real roots. First we need to find the real arguments f,g,h.
          ncase=6
! Table 2 Row 6
          coefs(1)=dcmplx(one,0.d0)
          coefs(2)=dcmplx(-cc/sqrt(ee),0.d0)
          coefs(3)=dcmplx(-one,0.d0)
          coefs(4)=dcmplx(sqrt(ee)*(two*cc/ee-(dd/ee)**2),0.d0)
          coefs(5)=dcmplx(-one,0.d0)
          coefs(6)=dcmplx(-cc/sqrt(ee),0.d0)
          coefs(7)=dcmplx(one,0.d0)
          call zroots(coefs,6,hroots,.true.)
          i=0
          h1=0.d0
10        continue
            i=i+1
            if(dimag(hroots(i)).eq.0.d0) h1=dble(hroots(i))
          if(h1.eq.0.d0) goto 10
          h2=one/h1
          g1=dd/ee/(h2-h1)
          g2=-g1
          f1=one/sqrt(ee)
          f2=f1
! Make sure there is a valid solution.
          if(su.gt.0.d0) then
            iut=elldoublecomplex(parr,f1,g1,h1,f2,g2,h2,0.d0,0.d0,dummy,u0,uplus)/sqrt(abs(ee))
          else
            iut=elldoublecomplex(parr,f1,g1,h1,f2,g2,h2,0.d0,0.d0,dummy,0.d0,u0)/sqrt(abs(ee))
          endif
          if(imu.gt.iut) then
            uf=-1.d0
            ncase=0
            tpr=0
            return
          endif
!         Next we want to write the real and imaginary parts of the roots m+/-in, p+/-ir in terms
!         of real quantities.
          coefs(1)=ee**(-three)
          coefs(2)=-cc/ee**three
          coefs(3)=-ee**(-two)
          coefs(4)=-ee**(-two)*(dd**two/ee-two*cc)
          coefs(5)=-one/ee
          coefs(6)=-cc/ee
          coefs(7)=one
          call zroots(coefs,6,hroots,.true.)
          i=0
          mn2=0.d0
20        continue
            i=i+1
            if(dimag(hroots(i)).eq.0.d0) mn2=dble(hroots(i))
          if(mn2.eq.0.d0) goto 20
          p=dd/(two*ee**2*(mn2**2-one/ee))
          m=-half*dd/ee-p
          if(m.lt.p) then
            temp=p
            p=m
            m=temp
          endif
          pr2=one/(mn2*ee)
          n=sqrt(mn2-m*m)
          r=sqrt(pr2-p*p)
          c4=sqrt((m-p)**2+(n+r)**2)
          c5=sqrt((m-p)**2+(n-r)**2)
          c1=(c4+c5)*half*sqrt(abs(ee))
          c2=sqrt((4.d0*n*n-(c4-c5)**2)/((c4+c5)**2-4.d0*n*n))
          c3=m+c2*n
          if(firstpt) iu0=sign(1.d0,(u0-c3))*elldoublecomplex(parr,f1,g1,h1,f2,g2,h2,0.d0,0.d0,dummy,c3,u0)/sqrt(abs(ee))
          m1=((c4-c5)/(c4+c5))**2
          jarg=c1*(su*imu+iu0)
          call sncndn(jarg,m1,sn,cn,dn)
          sc=sn/cn
          uf=c3+(n*(one+c2**2)*sc)/(one-c2*sc)
          if(pht) iu=elldoublecomplex(parr,f1,g1,h1,f2,g2,h2,0.d0,0.d0,rffu0,u0,uf)/sqrt(-ee)
        else
! These are the quartic real roots cases
          if(ncase.eq.7) then
! Table 2 Row 7
            if(firstpt.and.(u0.ne.u2)) &
             iu0=ellquarticreal(parr,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,0.d0,rffu0,u0,u2)
            jarg=sqrt(abs(ee)*(u3-u1)*(u4-u2))*half*(imu-su*iu0/sqrt(-ee))
            m1=(u4-u1)*(u3-u2)/((u4-u2)*(u3-u1))
            call sncndn(jarg,m1,sn,cn,dn)
            dummy=uf
            uf=((u2-u1)*u3*sn**2-u2*(u3-u1))/((u2-u1)*sn**2-(u3-u1))
            tpr=(sign(1.d0,su*(imu-su*iu0/sqrt(abs(ee))))+1.d0)/2.d0
            if(pht) iu1=ellquarticreal(parr,-u1,one,u2,-one,u3,-one,u4,-one,0.d0,0.d0,rffu1,uf,u2)/sqrt(-ee)
          elseif(ncase.eq.8) then
! Table 2 Row 8
            if(firstpt.and.(u0.ne.u3)) &
            iu0=ellquarticreal(parr,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,0.d0,rffu0,u3,u0)
            jarg=sqrt(abs(ee)*(u3-u1)*(u4-u2))*half*(imu+su*iu0/sqrt(-ee))
            m1=(u4-u1)*(u3-u2)/((u4-u2)*(u3-u1))
            call sncndn(jarg,m1,sn,cn,dn)
            uf=((u4-u3)*u2*sn**2-u3*(u4-u2))/((u4-u3)*sn**2-(u4-u2)) 
            tpr=(-sign(1.d0,su*(imu+su*iu0/sqrt(-ee)))+1.d0)/2.d0
            if(pht) iu1=ellquarticreal(parr,-u1,one,-u2,one,-u3,one,u4,-one,0.d0,0.d0,rffu1,u3,uf)/sqrt(-ee)
          endif
        endif
      endif
      return
      end
!******************************************************************************
      subroutine calcimuasym(a,mneg,mpos,mu0,muplus,imu0,imum,rf0,rfc)
!******************************************************************************
!     PURPOSE: Computes imu integral pieces involving only MU0 and MU turning points.
!
!     INPUTS:  A -- Black hole spin parameter.
!              MNEG, MPOS -- Roots of quadratic in \mu^2 M(\mu) in increasing order.
!              MU0 -- Initial \mu.
!              MUPLUS -- \sqrt{mpos}. Upper physical turning point.
!              RF0 -- RF term for Legendre elliptic integral of the first kind from MU0 to MUPLUS.
!              RFC -- RF term for complete Legendre elliptic integral of the first kind.              
!     OUTPUTS: IMU0 -- Piece of IMU integral from MU0 to MUPLUS
!              IMUM -- Piece of IMU integral from MUMINUS to MUPLUS
!     ROUTINES CALLED: *               
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      double precision a,mneg,mpos,mu0,muplus,imu0,imum
      double precision rf,m1,fac,phi0,s0,ak,q0,rf0,rfc
      fac=abs(a)*muplus
      m1=mneg/mpos
      phi0=asin(sqrt((mpos-mu0*mu0)/(mpos-mneg)))
      s0=(sqrt((mpos-mu0*mu0)/(mpos-mneg)))
      ak=sqrt(1.d0-m1)
      q0=(1.d0-s0*ak)*(1.d0+s0*ak)
      rf0=rf(1.d0-s0*s0,q0,1.d0)
      rfc=rf(0.d0,m1,1.d0)
      imum=2.d0*rfc
      imu0=s0*rf0
      if(tan(phi0).lt.0.d0) imu0=imum-imu0
      imu0=imu0/fac
      imum=imum/fac/2.d0
      return
      end
!******************************************************************************
      subroutine calcimuasymf(a,mneg,mpos,muf,muplus,imuf,imum,rff)
!******************************************************************************
!     PURPOSE: Computes imu integral pieces involving MUF in the asymmetric (M_- > 0) case.
!     INPUTS:  A -- Black hole spin parameter.
!              MNEG, MPOS -- Roots of quadratic in \mu^2 M(\mu) in increasing order.
!              MUF -- Final \mu.
!              MUPLUS -- \sqrt{mpos}. Upper physical turning point.
!              IMUM -- Piece of IMU integral from MUMINUS to MUPLUS.            
!     OUTPUTS: IMUF -- Piece of IMU integral from MU0 to MUPLUS
!              RFF -- RF term for IMUF.
!     ROUTINES CALLED: *               
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      double precision a,mneg,mpos,muplus,imum,muf,imuf
      double precision rf,m1,fac,ak,sf,qf,rff
      fac=abs(a)*muplus
      m1=mneg/mpos
      sf=(sqrt((mpos-muf*muf)/(mpos-mneg)))
      ak=sqrt(1.d0-m1)
      qf=(1.d0-sf*ak)*(1.d0+sf*ak)
      rff=rf(1.d0-sf*sf,qf,1.d0)
      imuf=sf*rff
      imuf=imuf/fac
      return
      end
!******************************************************************************
      subroutine calcimusym(a,mneg,mpos,mu0,muplus,imu0,imum,rf0,rfc)
!******************************************************************************
!     PURPOSE: Computes imu integral pieces involving only MU0 and MU turning points for 
!              the symmetric (M_- < 0) case.
!     INPUTS:  A -- Black hole spin parameter.
!              MNEG, MPOS -- Roots of quadratic in \mu^2 M(\mu) in increasing order.
!              MU0 -- Initial \mu.
!              MUPLUS -- \sqrt{mpos}. Upper physical turning point.
!              RF0 -- RF term for Legendre elliptic integral of the first kind from MU0 to MUPLUS.
!              RFC -- RF term for complete Legendre elliptic integral of the first kind.              
!     OUTPUTS: IMU0 -- Piece of IMU integral from MU0 to MUPLUS
!              IMUM -- Piece of IMU integral from MUMINUS to MUPLUS
!     ROUTINES CALLED: *               
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      double precision a,mneg,mpos,mu0,muplus,imu0,imum
      double precision rf,m1,fac,phi0,s0,ak,q0,rf0,rfc
      fac=abs(a)*sqrt(mpos-mneg)
      m1=-mneg/(mpos-mneg)
      phi0=acos(mu0/muplus)
      s0=sin(phi0)
      ak=sqrt(1.d0-m1)
      q0=(1.d0-s0*ak)*(1.d0+s0*ak)
      rf0=rf(1.d0-s0*s0,q0,1.d0)
      rfc=rf(0.d0,m1,1.d0)
      imum=2.d0*rfc
      imu0=s0*rf0
      if(tan(phi0).lt.0.d0) imu0=imum-imu0
      imu0=imu0/fac
      imum=imum/fac
      return
      end
!******************************************************************************
      subroutine calcimusymf(a,mneg,mpos,muf,muplus,imuf,imum,rff)
!******************************************************************************
!     PURPOSE: Computes imu integral pieces involving MUF in the asymmetric (M_- < 0) case.
!     INPUTS:  A -- Black hole spin parameter.
!              MNEG, MPOS -- Roots of quadratic in \mu^2 M(\mu) in increasing order.
!              MUF -- Final \mu.
!              MUPLUS -- \sqrt{mpos}. Upper physical turning point.
!              IMUM -- Piece of IMU integral from MUMINUS to MUPLUS.            
!     OUTPUTS: IMUF -- Piece of IMU integral from MU0 to MUPLUS.
!              RFF -- RF term for IMUF.
!     ROUTINES CALLED: *               
!     ACCURACY:   Machine.
!     REMARKS: Based on Dexter & Agol (2009), and labeled equations refer to that paper unless noted
!              otherwise.   
!     AUTHOR:     Dexter & Agol (2009)
!     DATE WRITTEN:  4 Mar 2009
!     REVISIONS: ***********************************************************************
      double precision a,mneg,mpos,muplus,imum,imuf,muf
      double precision rf,m1,fac,ak,sf,qf,phif,rff,rfc
      fac=abs(a)*sqrt(mpos-mneg)
      m1=-mneg/(mpos-mneg)
      phif=acos(muf/muplus)
      sf=sin(phif)
      ak=sqrt(1.d0-m1)
      qf=(1.d0-sf*ak)*(1.d0+sf*ak)
      rff=rf(1.d0-sf*sf,qf,1.d0)
      imuf=sf*rff
      if(tan(phif).lt.0.d0) imuf=imum*fac-imuf
      imuf=imuf/fac
      return
      end
!**********************************************************************
      subroutine SNCNDN(uu,emmc,sn,cn,dn)
!**********************************************************************
!     PURPOSE:  Compute Jacobi-elliptic functions SN,CN,DN.
!     ARGUMENTS:  Given the arguments U,EMMC=1-k^2 calculate sn(u,k), cn(u,k), dn(u,k).
!     REMARKS:  
!     AUTHOR:  Press et al (1992).
!     DATE WRITTEN:  25 Mar 91.
!     REVISIONS:
!**********************************************************************
      implicit none
      integer i,ii,l
      double precision a,b,c,ca,cn,d,dn,emc,emmc,sn,sqrt,u,uu
      parameter (ca=3.d-8)
      logical bo
      double precision em(13),en(13)
      emc=emmc
      u=uu
      if(emc.ne.0.d0)then
        bo=(emc.lt.0.d0)
        if(bo)then
          d=1.d0-emc
          emc=-emc/d
          d=sqrt(d)
          u=d*u
        endif
        a=1.d0
        dn=1.d0
        do 11 i=1,13
          l=i
          em(i)=a
          emc=sqrt(emc)
          en(i)=emc
          c=0.5d0*(a+emc)
          if(ABS(a-emc).le.ca*a)go to 1
          emc=a*emc
          a=c
11      continue
1       u=c*u
        sn=DSIN(u)
        cn=DCOS(u)
        if(sn.eq.0.)go to 2
        a=cn/sn
        c=a*c
        do 12 ii=l,1,-1
          b=EM(ii)
          a=c*a
          c=dn*c
          dn=(EN(ii)+a)/(b+a)
          a=c/b
12      continue
        a=1.d0/sqrt(c*c+1.d0)
        if(sn.lt.0.)then
          sn=-a
        else
          sn=a
        endif
        cn=c*sn
2       if(bo)then
          a=dn
          dn=cn
          cn=a
          sn=sn/d
        endif
      else
        cn=1.d0/DCOSH(u)
        dn=cn
        sn=DTANH(u)
      endif
      return
      end
!**********************************************************************
      subroutine ZROOTS(a,m,roots,polish)
!**********************************************************************
!     PURPOSE:  Find all roots of a polynomial.
!     ARGUMENTS:  Given the degree M and the M+1 complex coefficients
!       A of the polynomial (with A(0) being the constant term), this
!       routine returns all M roots in the complex array ROOTS.  The
!       logical variable POLISH should be input as .TRUE. if polishing
!       (by Laguerre's method) is desired, .FALSE. if the roots will be
!       subsequently polished by other means.
!     ROUTINES CALLED:  LAGUER.
!     ALGORITHM: Laguerre's method.
!     ACCURACY:  The parameter EPS sets the desired accuracy.
!     REMARKS:  
!     AUTHOR:  Press et al (1992).
!     DATE WRITTEN:  25 Mar 91.
!     REVISIONS:
!**********************************************************************
      integer m
      logical polish
!       
      integer i,j,jj,its,maxm,ncc
      double precision eps
      parameter (eps=1.d-6,maxm=7)
      complex*16 ad(maxm),x,b,c,xsum,xstart
      complex*16 a(maxm),roots(maxm-1)
!       
      if(m.gt.maxm-1) then
        write(6,*) 'M too large in ZROOTS'
      endif
!       Copy of coefficients for successive deflation.
      do 10 j=1,m+1
        ad(j)=A(j)
   10 continue
!       Loop over each root to be found.
      xsum=0.d0
       xstart=DCMPLX(0.D0,0.D0)
! If ncc=1, the previous root is a complex conjugate of another root
       ncc=0
      do 20 j=m,1,-1
! Start at zero to favour convergence to smallest remaining
! root, or if the previous root was complex, start at its complex
! conjugate (since the coefficients are real):
        x=xstart
         if(j.lt.m.and.dimag(ROOTS(j+1)).ne.0.d0.and.ncc.eq.0) then
           xstart=DCMPLX(dble(roots(j+1)),-dble(roots(j+1)))
! Since we have chosen the second root to start at the complex conjugate,
! we don't want to use its complex conjugate again as a starting root:
           ncc=1
         else
           xstart=DCMPLX(0.D0,0.D0)
           ncc=0
         endif
!        if(J.NE.1.or.a(M+1).eq.0.d0) then
! Find the root.
          call LAGUER(ad,j,x,its)
!           XSUM=XSUM+X
!        else
!          X=-a(M)/a(M+1)-XSUM
!        endif
        if(ABS(DIMAG(x)).le.2.d0*eps*eps*ABS(DBLE(x))) &
             x=DCMPLX(DBLE(x),0.d0)
        roots(j)=x
        b=AD(j+1)
!         Forward deflation.
        do 15 jj=j,1,-1
          c=AD(jj)
          ad(jj)=b
          b=x*b+c
   15   continue
   20 continue
      if(polish) then
!         Polish the roots using the undeflated coefficients.
        do 30 j=1,m
          call LAGUER(a,m,ROOTS(j),its)
   30   continue
      endif
      do 40 j=2,m
!         Sort roots by their real parts by straight insertion.
        x=ROOTS(j)
        do 35 i=j-1,1,-1
          if(DBLE(ROOTS(i)).le.DBLE(x)) go to 37
          roots(i+1)=ROOTS(i)
   35   continue
        i=0
   37   roots(i+1)=x
   40 continue
      return
      end
!***********************************************************************
       double precision function rf(x,y,z)
!***********************************************************************
!     PURPOSE: Compute Carlson fundamental integral RF
!              R_F=1/2 \int_0^\infty dt (t+x)^(-1/2) (t+y)^(-1/2) (t+z)^(-1/2)
!     ARGUMENTS: Symmetric arguments x,y,z
!     ROUTINES CALLED:  None.
!     ALGORITHM: Due to B.C. Carlson.
!     ACCURACY:  The parameter ERRTOL sets the desired accuracy.
!     REMARKS:  
!     AUTHOR:  Press et al (1992).
!     DATE WRITTEN:  25 Mar 91.
!     REVISIONS:
!**********************************************************************
       implicit none
       double precision x,y,z,errtol,tiny,big,third,a1,c2,c3,c4
       parameter(errtol=0.0025d0,tiny=1.5d-38,big=3.d37,third=1.d0/3.d0, &
                    a1=1.d0/24.d0,c2=0.1d0,c3=3.d0/44.d0,c4=1.d0/14.d0)
       double precision alamb,ave,delx,dely,delz,e2,e3,sqrtx,sqrty, &
                    sqrtz,sqrt,xt,yt,zt
       xt=x
       yt=y
       zt=z
1       continue
              sqrtx=sqrt(xt)
              sqrty=sqrt(yt)
              sqrtz=sqrt(zt)
              alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
              xt=0.25d0*(xt+alamb)
              yt=0.25d0*(yt+alamb)
              zt=0.25d0*(zt+alamb)
              ave=third*(xt+yt+zt)
              if(ave.eq.0.d0) then
                delx=0.d0
                dely=0.d0
                delz=0.d0
              else
                delx=(ave-xt)/ave
                dely=(ave-yt)/ave
                delz=(ave-zt)/ave
              endif
       if(max(abs(delx),abs(dely),abs(delz)).gt.errtol)go to 1
       e2=delx*dely-delz*delz
       e3=delx*dely*delz
       rf=(1.d0+(a1*e2-c2-c3*e3)*e2+c4*e3)/sqrt(ave)
       return
       end
       !********************************************************************************
       subroutine GAULEG(x1,x2,x,w,n,ns)
!******************************************************************************** 
!  PURPOSE: Subroutine to calculate the abscissas and weights for the Gauss-Legendre-
!  Quadrature. The routine is based on the NUMERICAL RECIPES and uses an
!  algorithem of G.B. Rybicki.
!  Input: x1 ,x2: range of integration.
!          n: order of the orthogonal polynomials and the quadrature formula.
!  Output: x = x(n): array of the abscissas.
!          w = w(n): array of the weights. 
      integer n,m,i,j
      real*8 x1,x2,x(ns),w(ns)
      real*8 pi,xm,xl,z,p1,p2,p3,z1,pp,eps
      parameter (pi = 3.14159265358979323846D0)
      parameter (eps=3.D-14)
 
      m=(n+1)/2
      xm=0.5D0*(x2+x1)
      xl=0.5D0*(x2-x1)
      do 12 i=1,m
         z=COS(pi*(i-.25D0)/(n+.5D0))
 1       continue
         p1=1.D0
         p2=0.D0
         do 11 j=1,n
            p3=p2
            p2=p1
            p1=((2.D0*j-1.D0)*z*p2-(j-1.D0)*p3)/j
 11      continue
         pp=n*(z*p1-p2)/(z*z-1.D0)
         z1=z
         z=z1-p1/pp
         if(ABS(z-z1).gt.eps) goto 1
         x(i)=xm-xl*z
         x(n+1-i)=xm+xl*z
         w(i)=2.D0*xl/((1.D0-z*z)*pp*pp)
         w(n+1-i)=W(i)
 12   continue
      return
      end
       !**********************************************************************
      subroutine LAGUER(a,m,x,its)
!**********************************************************************
!     PURPOSE:  Find one root of a polynomial.
!     ARGUMENTS:
!     ROUTINES CALLED:
!     ALGORITHM:  
!     ACCURACY:
!     REMARKS:  I don't have the documentation for this routine!
!     AUTHOR:  Press et al (1992)
!     DATE WRITTEN:  25 Mar 91.
!     REVISIONS:
!**********************************************************************
      implicit none
      integer its,m
      complex*16 a(m+1),x
!       
      integer iter,j,maxit,mr,mt
      parameter (mr=8,mt=10,maxit=MT*MR)
       double precision abx,abp,abm,err,epss,frac(mr) 
       parameter (epss=1.d-15)
      complex*16 dx,x1,b,d,f,g,h,sq,gp,gm,g2
      data FRAC /0.5d0,0.25d0,0.75d0,0.13d0,0.38d0,0.62d0,0.88d0,1.d0/
!       Loop over iterations up to allowed maximum.
      do 20 iter=1,maxit
!         
        its=iter
        b=A(m+1)
        err=ABS(b)
        d=DCMPLX(0.d0,0.d0)
        f=DCMPLX(0.d0,0.d0)
        abx=ABS(x)
        do 10 j=m,1,-1
!           Efficient computation of the polynomial and its first TWO
!           derivatives.
          f=x*f+d
          d=x*d+b
          b=x*b+A(j)
          err=ABS(b)+abx*err
   10   continue
        err=epss*err
!         
        if(ABS(b).le.err) then
!           Special case: we are on the root.
          return
        else
!           The generic case; use Laguerre's formula.
          g=d/b
          g2=g*g
          h=g2-2.d0*f/b
          sq=SQRT((m-1)*(m*h-g2))
          gp=g+sq
          gm=g-sq
          abp=ABS(gp)
          abm=ABS(gm)
          if(abp.lt.abm) gp=gm
          if (MAX(abp,abm).gt.0.d0) then
            dx=m/gp
          else
            dx=CEXP(CMPLX(LOG(1.d0+abx),DBLE(iter)))
          endif
        endif
        x1=x-dx
!         Check if we've converged.
        if(x.eq.x1)return
        if (MOD(iter,mt).ne.0) then
          x=x1
        else
!           
          x=x-dx*FRAC(iter/mt)
        endif
   20 continue
      write(6,*) 'Too many iterations'
      return
      end
!***********************************************************************
      double precision function rc(x,y)
!***********************************************************************
!     PURPOSE: Compute Carlson degenerate integral RC
!              R_C(x,y)=1/2 \int_0^\infty dt (t+x)^(-1/2) (t+y)^(-1)
!     ARGUMENTS: x,y
!     ROUTINES CALLED:  None.
!     ALGORITHM: Due to B.C. Carlson.
!     ACCURACY:  The parameter ERRTOL sets the desired accuracy.
!     REMARKS:  
!     AUTHOR:  Press et al (1992)
!     DATE WRITTEN:  25 Mar 91.
!     REVISIONS:
!**********************************************************************
      double precision x,y,errtol,tiny,sqrtny,big,tnbg,comp1,comp2,third,a1,c2, &
      c3,c4
      parameter (errtol=0.0012d0,tiny=1.69d-38,sqrtny=1.3d-19,big=3.d37, &
      tnbg=TINY*BIG,comp1=2.236d0/SQRTNY,comp2=TNBG*TNBG/25.d0,third=1.d0/3.d0, &
      a1=.3d0,c2=1.d0/7.d0,c3=.375d0,c4=9.d0/22.d0)
      double precision alamb,ave,s,w,xt,yt
         if(y.gt.0.)then
           xt=x
           yt=y
           w=1.d0
         else
           xt=x-y
           yt=-y
           w=sqrt(x)/sqrt(xt)
         endif
1        continue
           alamb=2.d0*sqrt(xt)*sqrt(yt)+yt
           xt=.25d0*(xt+alamb)
           yt=.25d0*(yt+alamb)
           ave=third*(xt+yt+yt)
           s=(yt-ave)/ave
         if(abs(s).gt.errtol)goto 1
         rc=w*(1.d0+s*s*(a1+s*(c2+s*(c3+s*c4))))/sqrt(ave)
      return
      end
!***********************************************************************
      double precision function rd(x,y,z)
!***********************************************************************
!     PURPOSE: Compute Carlson degenerate integral RD
!              R_D(x,y,z)=3/2 \int_0^\infty dt (t+x)^(-1/2) (t+y)^(-1/2) (t+z)^(-3/2)
!     ARGUMENTS: x,y,z
!     ROUTINES CALLED:  None.
!     ALGORITHM: Due to B.C. Carlson.
!     ACCURACY:  The parameter ERRTOL sets the desired accuracy.
!     REMARKS:  
!     AUTHOR:  Press et al (1992)
!     DATE WRITTEN:  25 Mar 91.
!     REVISIONS:
!**********************************************************************
      double precision x,y,z,errtol,tiny,big,a1,c2,c3,c4,c5,c6
      parameter (errtol=0.0015d0,tiny=1.d-25,big=4.5d21,a1=3.d0/14.d0,c2=1.d0/6.d0, &
      c3=9.d0/22.d0,c4=3.d0/26.d0,c5=0.25d0*C3,c6=1.5d0*C4)
      double precision alamb,ave,delx,dely,delz,ea,eb,ec,ed,ee,fac,sqrtx,sqrty, &
      sqrtz,sum,xt,yt,zt
      xt=x
      yt=y
      zt=z
      sum=0.d0
      fac=1.d0
1     continue
        sqrtx=sqrt(xt)
        sqrty=sqrt(yt)
        sqrtz=sqrt(zt)
        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
        sum=sum+fac/(sqrtz*(zt+alamb))
        fac=0.25d0*fac
        xt=.25d0*(xt+alamb)
        yt=.25d0*(yt+alamb)
        zt=.25d0*(zt+alamb)
        ave=.2d0*(xt+yt+3.d0*zt)
        delx=(ave-xt)/ave
        dely=(ave-yt)/ave
        delz=(ave-zt)/ave
      if(max(abs(delx),abs(dely),abs(delz)).gt.errtol)goto 1
      ea=delx*dely
      eb=delz*delz
      ec=ea-eb
      ed=ea-6.d0*eb
      ee=ed+ec+ec
      rd=3.d0*sum+fac*(1.d0+ed*(-a1+c5*ed-c6*delz*ee)+delz*(c2*ee+delz*(-c3* &
      ec+delz*c4*ea)))/(ave*sqrt(ave))
      return
      end
!***********************************************************************
      double precision function rj(x,y,z,p)
!***********************************************************************
!     PURPOSE: Compute Carlson fundamental integral RJ
!     RJ(x,y,z,p) = 3/2 \int_0^\infty dt
!                      (t+x)^(-1/2) (t+y)^(-1/2) (t+z)^(-1/2) (t+p)^(-1)
!     ARGUMENTS: x,y,z,p
!     ROUTINES CALLED:  RF, RC.
!     ALGORITHM: Due to B.C. Carlson.
!     ACCURACY:  The parameter ERRTOL sets the desired accuracy.
!     REMARKS:  
!     AUTHOR:  Press et al (1992)
!     DATE WRITTEN:  25 Mar 91.
!     REVISIONS:
!**********************************************************************      
      double precision p,x,y,z,errtol,tiny,big,a1,c2,c3,c4,c5,c6,c7,c8
      parameter (errtol=0.0015d0,tiny=2.5d-13,big=9.d11,a1=3.d0/14.d0,c2=1.d0/3.d0, &
      c3=3.d0/22.d0,c4=3.d0/26.d0,c5=.75d0*C3,c6=1.5d0*C4,c7=.5d0*C2,c8=C3+C3)
      double precision a,alamb,alpha,ave,b,beta,delp,delx,dely,delz,ea,eb,ec,ed,ee, &
      fac,pt,rcx,rho,sqrtx,sqrty,sqrtz,sum,tau,xt,yt,zt,rc,rf
        sum=0.d0
        fac=1.d0
        if(p.gt.0.d0)then
          xt=x
          yt=y
          zt=z
          pt=p
        else
          xt=min(x,y,z)
          zt=max(x,y,z)
          yt=x+y+z-xt-zt
          a=1.d0/(yt-p)
          b=a*(zt-yt)*(yt-xt)
          pt=yt+b
          rho=xt*zt/yt
          tau=p*pt/yt
          rcx=rc(rho,tau)
        endif
1       continue
          sqrtx=sqrt(xt)
          sqrty=sqrt(yt)
          sqrtz=sqrt(zt)
          alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
          alpha=(pt*(sqrtx+sqrty+sqrtz)+sqrtx*sqrty*sqrtz)**2
          beta=pt*(pt+alamb)**2
          sum=sum+fac*rc(alpha,beta)
          fac=.25d0*fac
          xt=.25d0*(xt+alamb)
          yt=.25d0*(yt+alamb)
          zt=.25d0*(zt+alamb)
          pt=.25d0*(pt+alamb)
          ave=.2d0*(xt+yt+zt+pt+pt)
          delx=(ave-xt)/ave
          dely=(ave-yt)/ave
          delz=(ave-zt)/ave
          delp=(ave-pt)/ave
        if(max(abs(delx),abs(dely),abs(delz),abs(delp)).gt.errtol)goto 1
        ea=delx*(dely+delz)+dely*delz
        eb=delx*dely*delz
        ec=delp**2
        ed=ea-3.d0*ec
        ee=eb+2.d0*delp*(ea-ec)
        rj=3.d0*sum+fac*(1.d0+ed*(-a1+c5*ed-c6*ee)+eb*(c7+delp*(-c8+delp*c4))+ &
        delp*ea*(c2-delp*c3)-c2*delp*ec)/(ave*sqrt(ave))
        if (p.le.0.d0) rj=a*(b*rj+3.d0*(rcx-rf(xt,yt,zt)))
      return
      end
